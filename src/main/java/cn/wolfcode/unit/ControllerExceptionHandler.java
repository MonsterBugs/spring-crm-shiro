package cn.wolfcode.unit;

import cn.wolfcode.query.JsonResult;
import com.alibaba.fastjson.JSON;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@ControllerAdvice   //使用控制器增强事务功能,提供统一controller类的异常处理
public class ControllerExceptionHandler {
    //指定捕获的异常类型
    @org.springframework.web.bind.annotation.ExceptionHandler(RuntimeException.class)
    public String handleException(RuntimeException ex, HandlerMethod handlerMethod, HttpServletResponse response) throws ServletException, IOException {
        //异常信息打印出来,方便找bug
        ex.printStackTrace();
        //判断当前执行的方法是否是ajax的方法(@ResponseBody)
        if (handlerMethod.hasMethodAnnotation(ResponseBody.class)) {
            //如果是,则返回jsonresult
            JsonResult json = new JsonResult(false, "操作失败,请联系管理员!");
            //使用response对象输出数据(json字符串) 一定要设置编码格式
            response.setContentType("application/json;charset=utf-8");
            response.getWriter().print(JSON.toJSONString(json));
            return null;
        }
        //如果不是,则返回错误视图
        return "common/error"; //返回的错误视图
    }
    //捕获指定权限或角色时,
    @org.springframework.web.bind.annotation.ExceptionHandler(UnauthorizedException.class)
    public String unauthorizedException(RuntimeException ex, HandlerMethod handlerMethod, HttpServletResponse response) throws ServletException, IOException {
        //异常信息打印出来,方便找bug
        ex.printStackTrace();
        //判断当前执行的方法是否是ajax的方法(@ResponseBody)
        if (handlerMethod.hasMethodAnnotation(ResponseBody.class)) {
            //如果是,则返回jsonresult
            JsonResult json = new JsonResult(false, "您没有执行该操作的权限,请联系管理员!");
            //使用response对象输出数据(json字符串) 一定要设置编码格式
            response.setContentType("application/json;charset=utf-8");
            response.getWriter().print(JSON.toJSONString(json));
            return null;
        }
        //如果不是,则返回错误视图
        return "common/error"; //返回的错误视图
    }


}
