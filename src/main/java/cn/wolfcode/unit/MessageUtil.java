package cn.wolfcode.unit;
import cn.wolfcode.query.CustomerReportQuery;
//处理分组类型显示问题的工具类
public abstract class MessageUtil {
    public static String changMsg(CustomerReportQuery qo) {
        String msg = null;
        switch (qo.getGroupType()) {
            case "DATE_FORMAT(c.input_time, '%Y')":
                msg = "年份";
                break;
            case "DATE_FORMAT(c.input_time, '%Y-%m')":
                msg = "月份";
                break;
            case "DATE_FORMAT(c.input_time, '%Y-%m-%d')":
                msg = "日期";
                break;
            default:
                msg = "员工";
        }
        return msg;
    }
}
