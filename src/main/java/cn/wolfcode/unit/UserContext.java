package cn.wolfcode.unit;

import cn.wolfcode.domain.Employee;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

public class UserContext {
    private UserContext() {
    }

    //获取当前用户
    public static Employee getUser() {
        Employee employee = (Employee) SecurityUtils.getSubject().getPrincipal();
        return employee;
    }
}
