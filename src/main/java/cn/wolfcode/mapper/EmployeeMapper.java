package cn.wolfcode.mapper;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.query.EmployeeQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Employee employee);

    Employee selectByPrimaryKey(Long id);

    List<Employee> list(EmployeeQuery qo);

    List<Employee> selectAll();

    int updateByPrimaryKey(Employee employee);

    /**
     * 保存员工与角色的关系
     *
     * @param id  员工id
     * @param ids 员工角色id数组
     */
    void insertRealtion(@Param("id") Long id, @Param("ids") long[] ids);

    /**
     * 更新员工与角色的关系
     *
     * @param id  员工id
     * @param ids 员工角色id数组
     */
    void updatetRealtion(@Param("id") Long id, @Param("ids") long[] ids);

    /**
     * 更新员工与角色的关系
     *
     * @param id 员工id
     */
    void deleteRealtion(@Param("id") Long id);

    Employee login(@Param("username") String username, @Param("password") String password);

    Employee checkupdatePwd(@Param("id") Long id, @Param("oldPassword") String oldPassword);

    void updatePwd(@Param("name") String name, @Param("newPassword") String newPassword);

    List<String> selectExpressionById(Long id);

    void batchDelete(Long[] ids);

    void batchDeleteRealtion(Long[] ids);

    Employee selectByNameId(@Param("name") String name, @Param("id") long id);

    Employee selectByName(@Param("name") String name);

    void changeStatus(@Param("id") Long id, @Param("status") boolean status);

    List<Employee> selectByRoleSn(String... roleSn);

    void noticeRelation(@Param("noticeId") Long noticeId,@Param("id") Long id);
}