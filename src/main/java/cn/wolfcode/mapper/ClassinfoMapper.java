package cn.wolfcode.mapper;

import cn.wolfcode.domain.Classinfo;
import cn.wolfcode.query.ClassinfoQuery;

import java.util.List;

public interface ClassinfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Classinfo record);

    Classinfo selectByPrimaryKey(Long id);

    List<Classinfo> selectAll();

    int updateByPrimaryKey(Classinfo record);

    List<Classinfo> list(ClassinfoQuery qo);
        
            
                
                     
}