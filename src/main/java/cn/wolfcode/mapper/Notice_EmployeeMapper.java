package cn.wolfcode.mapper;

import cn.wolfcode.domain.Notice_Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface Notice_EmployeeMapper {
    int insert(Notice_Employee record);

    List<Notice_Employee> selectAll();

    Notice_Employee selectByEmployeeIdNoticeId(@Param("employeeId") Long employeeId, @Param("noticeId") Long noticeId);
}