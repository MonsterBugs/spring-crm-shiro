package cn.wolfcode.mapper;

import cn.wolfcode.domain.Permission;
import cn.wolfcode.query.Query;

import java.util.List;

public interface PermissionMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Permission record);

    Permission selectByPrimaryKey(Long id);

    List<Permission> selectByRoleId(long id);

    int updateByPrimaryKey(Permission record);

    List<Permission> listAll();

    List<Permission> list(Query qo);

    List<String>listAllExpression();
}
