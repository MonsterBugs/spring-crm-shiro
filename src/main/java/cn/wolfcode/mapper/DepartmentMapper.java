package cn.wolfcode.mapper;

import cn.wolfcode.domain.Department;
import cn.wolfcode.query.Query;

import java.util.List;

public interface DepartmentMapper {
    //删除
    int deleteByPrimaryKey(Long id);
    //保存
    int insert(Department record);
    //查询
    Department selectByPrimaryKey(Long id);
    //更新
    int updateByPrimaryKey(Department record);
    //分页
    List<Department> list(Query qo);
    //查询全部
    List<Department> listAll();

}