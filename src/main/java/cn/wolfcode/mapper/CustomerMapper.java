package cn.wolfcode.mapper;

import cn.wolfcode.domain.Customer;
import cn.wolfcode.query.CustomerQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Customer record);

    Customer selectByPrimaryKey(Long id);

    List<Customer> selectAll();

    int updateByPrimaryKey(Customer record);

    List<Customer> selectForList(CustomerQuery qo);

    Customer selectByTelId(@Param("tel") String tel, @Param("id") Long id);

    Customer selectByTel(String tel);

    void changeStatus(@Param("id") Long id, @Param("status") Integer status);

    void updateSeller(@Param("customerId") Long customerId, @Param("sellerId") Long sellerId);
}