package cn.wolfcode.mapper;

import cn.wolfcode.query.CustomerReportQuery;

import java.util.HashMap;
import java.util.List;

//根据分组条件(销售员的名字/年月/日),查询对应潜在客户数量(status=0)
public interface CustomerReportMapper {
    List<HashMap>  selectCustomerReport(CustomerReportQuery qo);
}
