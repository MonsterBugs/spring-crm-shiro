package cn.wolfcode.mapper;

import cn.wolfcode.domain.Role;
import cn.wolfcode.query.Query;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Role role);

    Role selectByPrimaryKey(Long id);

    int updateByPrimaryKey(Role role);

    //分页
    List<Role> list(Query qo);

    //全部
    List<Role> listAll();

    //根据角色id查询对应角色
    List<Role> selectByEmpId(long id);
    //维护角色与权限关系

    /**
     * 保存角色与角色的关系
     *
     * @param id  角色id
     * @param ids 角色的权限id数组
     */
    void insertRealtion(@Param("id") Long id, @Param("ids") long[] ids);

    /**
     * 更新角色与角色的关系
     *
     * @param id  角色id
     * @param ids 角色的权限id数组
     */
    void updatetRealtion(@Param("id") Long id, @Param("ids") long[] ids);

    /**
     * 更新角色与角色的关系
     *
     * @param id 角色id
     */
    void deleteRealtion(@Param("id") Long id);


}