package cn.wolfcode.mapper;

import cn.wolfcode.domain.SystemDictionaryItem;
import cn.wolfcode.query.Query;
import java.util.List;

public interface systemDictionaryItemMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SystemDictionaryItem record);

    SystemDictionaryItem selectByPrimaryKey(Long id);

    List<SystemDictionaryItem> selectAll();

    int updateByPrimaryKey(SystemDictionaryItem record);

    List<SystemDictionaryItem> selectForList(Query qo);

    int MaxSequenceByparent_id(Long parentId);

    List<SystemDictionaryItem> selectByParentId(long parentId);
}