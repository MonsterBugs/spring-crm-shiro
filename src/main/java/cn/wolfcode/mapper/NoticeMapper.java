package cn.wolfcode.mapper;

import cn.wolfcode.domain.Notice;
import cn.wolfcode.query.Query;
import java.util.List;

public interface NoticeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Notice record);

    Notice selectByPrimaryKey(Long id);

    List<Notice> selectAll();

    int updateByPrimaryKey(Notice record);

    List<Notice> selectForList(Query qo);
}