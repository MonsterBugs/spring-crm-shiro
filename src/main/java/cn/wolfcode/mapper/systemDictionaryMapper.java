package cn.wolfcode.mapper;

import cn.wolfcode.domain.SystemDictionary;
import cn.wolfcode.query.Query;

import java.util.List;

public interface systemDictionaryMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SystemDictionary record);

    SystemDictionary selectByPrimaryKey(Long id);

    List<SystemDictionary> selectAll();

    int updateByPrimaryKey(SystemDictionary record);

    List<SystemDictionary> selectForList(Query qo);
}