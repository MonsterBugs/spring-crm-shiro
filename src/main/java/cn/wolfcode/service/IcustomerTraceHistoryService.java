package cn.wolfcode.service;

import cn.wolfcode.domain.CustomerTraceHistory;
import cn.wolfcode.query.Query;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IcustomerTraceHistoryService {
    void save(CustomerTraceHistory customerTraceHistory);
    void delete(Long id);
    void update(CustomerTraceHistory customerTraceHistory);
    CustomerTraceHistory get(Long id);
    List<CustomerTraceHistory> listAll();
    // 分页查询的方法
    PageInfo<CustomerTraceHistory> query(Query qo);
}
