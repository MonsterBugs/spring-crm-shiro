package cn.wolfcode.service;

import cn.wolfcode.domain.Customer;
import cn.wolfcode.domain.Employee;
import cn.wolfcode.query.CustomerQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface ICustomerService {
    void save(Customer customer);

    void delete(Long id);

    void update(Customer customer);

    Customer get(Long id);

    List<Customer> listAll();

    // 分页查询的方法
    PageInfo<Customer> query(CustomerQuery qo);

    Customer selectByTelId(String tel, Long id);

    Customer selectByTel(String tel);

    void changeStatus(Long id, Integer status);
}
