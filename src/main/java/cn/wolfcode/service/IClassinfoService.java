package cn.wolfcode.service;

import cn.wolfcode.domain.Classinfo;
import cn.wolfcode.query.ClassinfoQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IClassinfoService {
    void deleteByPrimaryKey(Long id);

    void insert(Classinfo record);

    Classinfo selectByPrimaryKey(Long id);

    List<Classinfo> selectAll();

    void updateByPrimaryKey(Classinfo record);

    PageInfo<Classinfo> queryForResult(ClassinfoQuery qo);
}

