package cn.wolfcode.service;

import cn.wolfcode.domain.Role;
import cn.wolfcode.query.Query;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IRoleService {
    //删除
    void deleteByPrimaryKey(Long id);
    //保存
    void insert(Role role,long[]ids);
    //查询
    Role selectByPrimaryKey(Long id);
    //更新
    void updateByPrimaryKey(Role role,long[]ids);
    //分页结果
    PageInfo queryForResult(Query qo);
    //全部
    List<Role> listAll();
}
