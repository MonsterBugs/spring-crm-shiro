package cn.wolfcode.service;

import cn.wolfcode.query.CustomerReportQuery;
import com.github.pagehelper.PageInfo;

import java.util.HashMap;
import java.util.List;

public interface ICustomerReportService {
    //根据高查条件和分组类型查询客户数
    public PageInfo selectCustomerReport(CustomerReportQuery qo);

    //根据高查条件和分组类型查询客户数,返回柱状图,不用分页
    public List<HashMap> selectAll(CustomerReportQuery qo);
}
