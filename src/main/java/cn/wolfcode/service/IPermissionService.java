package cn.wolfcode.service;

import cn.wolfcode.domain.Permission;
import cn.wolfcode.query.Query;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IPermissionService {
    //删除
    void deleteByPrimaryKey(Long id);
    //保存
    void insert(Permission permission);
    //查询
    Permission selectByPrimaryKey(Long id);
    //更新
    void updateByPrimaryKey(Permission permission);
    //分页结果
    PageInfo queryForResult(Query qo);
    //全部
    List<Permission> listAll();

    void reload();

}
