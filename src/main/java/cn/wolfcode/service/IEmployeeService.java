package cn.wolfcode.service;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.query.EmployeeQuery;
import cn.wolfcode.query.JsonResult;
import com.github.pagehelper.PageInfo;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface IEmployeeService {
    //删除
    void deleteByPrimaryKey(Long id);

    //保存
    void insert(Employee Employee, long[] ids);

    //查询
    Employee selectByPrimaryKey(Long id);

    //更新
    void updateByPrimaryKey(Employee Employee, long[] ids);

    //分页高查结果
    PageInfo<Employee> queryForResult(EmployeeQuery qo);

    //员工登录
    Employee login(String username, String password);

    Employee checkoldPassword(Long id, String oldPassword);

    JsonResult updatePwd(String name, String newPassword);

    List<String> selectExpressionById(Long id);

    void batchDelete(Long[] ids);

    List<Employee> selectAll();

    Employee selectByNameId(String name, Long id);

    Employee selectByName(String name);

    /**
     * 返回员工密码已加密的员工对象
     * @param employee
     * @return
     */
    Employee MD5Employee(Employee employee);

    void changeStatus(Long id, boolean status);

    void restPwd(String name, String newPassWordMd5);

    Workbook exportXls();

    void importXls(MultipartFile multipartFile) throws IOException;
    //根据角色编码查询员工
    List<Employee> selectByRoleSn(String... roleSn);
}
