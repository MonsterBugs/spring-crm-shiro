package cn.wolfcode.service;

import cn.wolfcode.domain.Department;
import cn.wolfcode.query.Query;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IDepartmentService {
    //删除
    void deleteByPrimaryKey(Long id);
    //保存
    void insert(Department department);
    //查询
    Department selectByPrimaryKey(Long id);
    //更新
    void updateByPrimaryKey(Department department);
    //分页结果
    PageInfo<Department> queryForResult(Query qo);
    //全部
    List<Department> listAll();
}
