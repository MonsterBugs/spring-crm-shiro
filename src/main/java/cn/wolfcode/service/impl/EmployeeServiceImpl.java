package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.mapper.EmployeeMapper;
import cn.wolfcode.query.EmployeeQuery;
import cn.wolfcode.query.JsonResult;
import cn.wolfcode.service.IEmployeeService;
import cn.wolfcode.unit.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
@Transactional
public class EmployeeServiceImpl implements IEmployeeService {
    @Autowired
    private EmployeeMapper employeemapper;

    @Override
    public void deleteByPrimaryKey(Long id) {
        //1.先删除员工-角色关系
        employeemapper.deleteRealtion(id);
        System.out.println(1 / 0);
        //2.删除员工
        employeemapper.deleteByPrimaryKey(id);
    }

    @Override
    public void insert(Employee employee, long[] ids) {
        //判断ids是否有值;
        if (ids != null && ids.length != 0) {
            //保存员工-角色关系
            employeemapper.insertRealtion(employee.getId(), ids);
        }
        //返回员工密码已加密的员工对象
        Employee MD5Employee = MD5Employee(employee);
        employeemapper.insert(MD5Employee);

    }

    @Override
    public Employee selectByPrimaryKey(Long id) {
        return employeemapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateByPrimaryKey(Employee employee, long[] ids) {
        //删除员工与角色
        employeemapper.deleteRealtion(employee.getId());
        //并ids判断是否有值;
        if (ids != null && ids.length != 0) {
            //更新员工-角色关系
            employeemapper.insertRealtion(employee.getId(), ids);
        }
        //更新员工信息
        employeemapper.updateByPrimaryKey(employee);
    }

    @Override
    public PageInfo<Employee> queryForResult(EmployeeQuery qo) {
        //设置当前页和每页条数
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageCount());
        List<Employee> list = employeemapper.list(qo);
        System.out.println("你好");
        return new PageInfo(list);
    }

    @Override
    public Employee login(String employeename, String password) {
        return employeemapper.login(employeename, password);


    }

    @Override
    public Employee checkoldPassword(Long id, String oldPassword) {
        return employeemapper.checkupdatePwd(id, oldPassword);
    }

    @Override
    public JsonResult updatePwd(String oldPassword, String newPassword) {
        //获取当前登录用户主体对象
        Employee login = UserContext.getUser();
        //获取用户名
        String name = login.getName();
        //使用账户名作为盐,进行旧密码加密,
        String oldPassWordMd5 = new Md5Hash(oldPassword, name).toString();
        //判断员工的旧密码是否正确
        Employee employee = employeemapper.login(name, oldPassWordMd5);
        if (employee != null) {
            //使用账户名作为盐,进行新密码加密,
            String newPassWordMd5 = new Md5Hash(newPassword,
                    name).toString();
            //更新密码
            employeemapper.updatePwd(name, newPassWordMd5);
            return new JsonResult(true, "修改成功");
        }
        return new JsonResult(false, "原密码错误,请重新核对");
    }

    @Override
    public List<String> selectExpressionById(Long id) {
        return employeemapper.selectExpressionById(id);
    }

    @Override
    public void batchDelete(Long[] ids) {
        employeemapper.batchDeleteRealtion(ids);
        employeemapper.batchDelete(ids);
    }

    @Override
    public List<Employee> selectAll() {
        return employeemapper.selectAll();
    }

    @Override
    public Employee selectByNameId(String name, Long id) {
        return employeemapper.selectByNameId(name, id);
    }

    @Override
    public Employee selectByName(String name) {
        return employeemapper.selectByName(name);
    }

    @Override
    public Employee MD5Employee(Employee employee) {
        //获取员工的明文密码
        String password = employee.getPassword();
        //使用员工名作为加密的盐
        String salt = employee.getName();
        //明文密码加密
        String passWordMd5 = new Md5Hash(password, salt).toString();
        //设置员工加密密码
        employee.setPassword(passWordMd5);
        return employee;
    }

    @Override
    public void changeStatus(Long id, boolean status) {
        employeemapper.changeStatus(id, status);
    }


    @Override
    public Workbook exportXls() {
        //创建workbook对象
        Workbook workbook = new HSSFWorkbook();
        //创建表
        Sheet sheet = workbook.createSheet("员工联系表");
        //标题行
        Row row = sheet.createRow(0);
        //给标题行的单元格设置内容
        row.createCell(0).setCellValue("姓名");
        row.createCell(1).setCellValue("年龄");
        row.createCell(2).setCellValue("邮箱");
        //查询所有员工数据
        List<Employee> employees = employeemapper.selectAll();
        //遍历员工数据,将数据设添加表中
        for (int i = 0; i < employees.size(); i++) {
            Employee employee = employees.get(i);
            //内容行
            Row Erow = sheet.createRow(i + 1);
            //给标题行的单元格设置内容
            Erow.createCell(0).setCellValue(employee.getName());
            Erow.createCell(1).setCellValue(employee.getAge());
            Erow.createCell(2).setCellValue(employee.getEmail());
        }
        return workbook;
    }

    @Override
    public void importXls(MultipartFile multipartFile) throws IOException {
        //读取xls表格数据
        Workbook workbook = new HSSFWorkbook(multipartFile.getInputStream());
        //读取第一张表
        Sheet sheetAt = workbook.getSheetAt(0);
        //获取最后一行的索引
        int lastRowNum = sheetAt.getLastRowNum();
        //从第2行(跳过标题行)遍历表
        for (int i = 1; i <= lastRowNum; i++) {
            Employee e = new Employee();
            Row row = sheetAt.getRow(i);
            String name = row.getCell(0).getStringCellValue();
            //如果用户名为空,停止往下读
            if (!StringUtils.hasLength(name.trim())) {
                return;
            }
            e.setName(name);
            double age = row.getCell(1).getNumericCellValue();
            e.setAge(Integer.valueOf((int) age));
            String email = row.getCell(2).getStringCellValue();
            e.setEmail(email);
            //设置员工密码
            e.setPassword("1");
            //保存员工
            insert(e, null);
        }
    }

    @Override
    public List<Employee> selectByRoleSn(String... roleSn) {
        return employeemapper.selectByRoleSn(roleSn);
    }

    @Override
    public void restPwd(String name, String newPassword) {
        //新密码加密
        String newPassWordMd5 = new Md5Hash(newPassword, name).toString();
        //更新密码
        employeemapper.updatePwd(name, newPassWordMd5);
    }
}

