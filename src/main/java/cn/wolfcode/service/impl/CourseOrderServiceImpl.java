package cn.wolfcode.service.impl;

import cn.wolfcode.domain.CourseOrder;
import cn.wolfcode.mapper.CourseOrderMapper;
import cn.wolfcode.query.Query;
import cn.wolfcode.service.ICourseOrderService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CourseOrderServiceImpl implements ICourseOrderService {

    @Autowired
    private CourseOrderMapper courseOrderMapper;


    @Override
    public void save(CourseOrder courseOrder) {
        //设置销售时间
        courseOrder.setInputTime(new Date());
        courseOrderMapper.insert(courseOrder);
    }

    @Override
    public void delete(Long id) {
        courseOrderMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(CourseOrder courseOrder) {
        courseOrderMapper.updateByPrimaryKey(courseOrder);
    }

    @Override
    public CourseOrder get(Long id) {
        return courseOrderMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<CourseOrder> listAll() {
        return courseOrderMapper.selectAll();
    }

    @Override
    public PageInfo<CourseOrder> query(Query qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageCount()); //对下一句sql进行自动分页
        List<CourseOrder> courseOrders = courseOrderMapper.selectForList(qo); //里面不需要自己写limit
        return new PageInfo<CourseOrder>(courseOrders);
    }
}
