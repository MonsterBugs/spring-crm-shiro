package cn.wolfcode.service.impl;

import cn.wolfcode.domain.CustomerTraceHistory;
import cn.wolfcode.domain.Employee;
import cn.wolfcode.mapper.customerTraceHistoryMapper;
import cn.wolfcode.query.Query;
import cn.wolfcode.service.IcustomerTraceHistoryService;
import cn.wolfcode.unit.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CustomerTraceHistoryServiceImpl implements IcustomerTraceHistoryService {

    @Autowired
    private customerTraceHistoryMapper customerTraceHistoryMapper;


    @Override
    public void save(CustomerTraceHistory customerTraceHistory) {
        //获取当前登录用户
        Employee employee = UserContext.getUser();;
        //设置录入人
        customerTraceHistory.setInputUser(employee);
        //设置录入时间
        customerTraceHistory.setInputTime(new Date());
        customerTraceHistoryMapper.insert(customerTraceHistory);
        customerTraceHistoryMapper.insert(customerTraceHistory);
    }

    @Override
    public void delete(Long id) {
        customerTraceHistoryMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(CustomerTraceHistory customerTraceHistory) {
        customerTraceHistoryMapper.updateByPrimaryKey(customerTraceHistory);
    }

    @Override
    public CustomerTraceHistory get(Long id) {
        return customerTraceHistoryMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<CustomerTraceHistory> listAll() {
        return customerTraceHistoryMapper.selectAll();
    }


    @Override
    public PageInfo<CustomerTraceHistory> query(Query qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageCount()); //对下一句sql进行自动分页
        List<CustomerTraceHistory> customerTraceHistorys = customerTraceHistoryMapper.selectForList(qo); //里面不需要自己写limit
        return new PageInfo<CustomerTraceHistory>(customerTraceHistorys);
    }
}
