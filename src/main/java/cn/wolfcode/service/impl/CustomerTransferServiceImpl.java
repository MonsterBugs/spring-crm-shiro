package cn.wolfcode.service.impl;

import cn.wolfcode.domain.CustomerTransfer;
import cn.wolfcode.domain.Employee;
import cn.wolfcode.mapper.CustomerMapper;
import cn.wolfcode.mapper.CustomerTransferMapper;
import cn.wolfcode.query.Query;
import cn.wolfcode.service.ICustomerTransferService;
import cn.wolfcode.unit.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CustomerTransferServiceImpl implements ICustomerTransferService {

    @Autowired
    private CustomerTransferMapper customerTransferMapper;
    @Autowired
    private CustomerMapper customerMapper;

    @Override
    public void save(CustomerTransfer customerTransfer) {
        //客户的销售员改为新的销售人员
        Long customerId = customerTransfer.getCustomer().getId();
        Long sellerId = customerTransfer.getNewSeller().getId();
        customerMapper.updateSeller(customerId,sellerId);
        //保存操作历史
        //设置操作人
        customerTransfer.setOperator(UserContext.getUser());
        //设置操作时间
        customerTransfer.setOperateTime(new Date());
        customerTransferMapper.insert(customerTransfer);
        customerTransferMapper.insert(customerTransfer);
    }

    @Override
    public void delete(Long id) {
        customerTransferMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(CustomerTransfer customerTransfer) {
        customerTransferMapper.updateByPrimaryKey(customerTransfer);
    }

    @Override
    public CustomerTransfer get(Long id) {
        return customerTransferMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<CustomerTransfer> listAll() {
        return customerTransferMapper.selectAll();
    }

    @Override
    public PageInfo<CustomerTransfer> query(Query qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageCount()); //对下一句sql进行自动分页
        List<CustomerTransfer> customerTransfers = customerTransferMapper.selectForList(qo); //里面不需要自己写limit
        return new PageInfo<CustomerTransfer>(customerTransfers);
    }
}
