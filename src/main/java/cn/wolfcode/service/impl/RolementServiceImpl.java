package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Role;
import cn.wolfcode.mapper.RoleMapper;
import cn.wolfcode.query.Query;
import cn.wolfcode.service.IRoleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RolementServiceImpl implements IRoleService {
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public void deleteByPrimaryKey(Long id) {
        //1.先删除角色的权限
        roleMapper.deleteRealtion(id);
        //2.删除角色
        roleMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void insert(Role role, long[] ids) {
        roleMapper.insert(role);
        //维护角色与权限的关系前,判断ids是否有值;
        if (ids != null&&ids.length!=0) {
            roleMapper.insertRealtion(role.getId(),ids);
        }

    }

    @Override
    public Role selectByPrimaryKey(Long id) {
        return roleMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateByPrimaryKey(Role role, long[] ids) {
        //删除角色以前的权限
        roleMapper.deleteRealtion(role.getId());
        //添加角色新的权限,并判断是否有值;
        if (ids != null&&ids.length!=0) {
            roleMapper.insertRealtion(role.getId(),ids);
        }
        //保存角色其他信息
        roleMapper.updateByPrimaryKey(role);
    }

    @Override
    public PageInfo queryForResult(Query qo) {
        //设置当前页和每页条数
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageCount());
        List<Role> list = roleMapper.list(qo);
        return new PageInfo(list);
    }


    //全部
    public List<Role> listAll() {
        return roleMapper.listAll();
    }
}

