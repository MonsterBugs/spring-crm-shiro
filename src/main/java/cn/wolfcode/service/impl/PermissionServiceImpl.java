package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Permission;
import cn.wolfcode.mapper.PermissionMapper;
import cn.wolfcode.query.Query;
import cn.wolfcode.service.IPermissionService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class PermissionServiceImpl implements IPermissionService {
    @Autowired
    private PermissionMapper permissionMapper;
    @Autowired
    private ApplicationContext atxt;    //Spring容器所有bean对象

    @Override
    public void deleteByPrimaryKey(Long id) {
        permissionMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void insert(Permission permission) {
        permissionMapper.insert(permission);
    }

    @Override
    public Permission selectByPrimaryKey(Long id) {
        return permissionMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateByPrimaryKey(Permission permission) {
        permissionMapper.updateByPrimaryKey(permission);
    }

    @Override
    public PageInfo queryForResult(Query qo) {
        //设置当前页和每页条数
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageCount());
        List<Permission> list = permissionMapper.list(qo);
        return new PageInfo(list);
    }

    //全部
    @Override
    public List<Permission> listAll() {
        return permissionMapper.listAll();
    }

    //重新加载
    @Override
    public void reload() {
        //查询全部权限表达式
        List<String> expressions = permissionMapper.listAllExpression();
        //从spring容器中获取获取所有的controller
        Map<String, Object> controllers = atxt.getBeansWithAnnotation(Controller.class);
        //获取所有value值(Controller的字节码对象)
        Collection<Object> values = controllers.values();
        System.out.println(values + "------------");
        //遍历循环获取每一个controller
        for (Object controller : values) {
            //判断运行时期,运行的是cotroller的真实类,还是其代理类
            if (AopUtils.isCglibProxy(controller)) {
                /*获取controller的字节码对象的真实类字节码对象(使用shiro注解,运行时期,是运行shiro生成的代理类,
                而代理类与真实类是继承关系,无法继承真实类方法上的注解)*/
                Class<?> clzz = controller.getClass().getSuperclass();
                //获取controller的所有方法
                Method[] methods = clzz.getDeclaredMethods();
                //遍历出每一个方法
                for (Method method : methods) {
                    RequiresPermissions annotation = method.getAnnotation(RequiresPermissions.class);
                    //判断是否有贴自定义的权限注解
                    if (annotation != null) {
                        //通过注解获取
                        String expression = annotation.value()[0];    //获取权限表达式
                        String name = annotation.value()[1];    //获取权限对象的name
                        //判断权限表达式是否已经存在数据库中,如果不存在,就需要插入
                        if (!expressions.contains(expression)) {
                            permissionMapper.insert(new Permission(null, name, expression));
                        }
                    }


                }
            }


        }

    }
}

