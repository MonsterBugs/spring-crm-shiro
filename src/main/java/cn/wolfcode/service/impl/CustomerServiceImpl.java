package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Customer;
import cn.wolfcode.domain.Employee;
import cn.wolfcode.mapper.CustomerMapper;
import cn.wolfcode.query.CustomerQuery;
import cn.wolfcode.service.ICustomerService;
import cn.wolfcode.unit.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CustomerServiceImpl implements ICustomerService {

    @Autowired
    private CustomerMapper customerMapper;


    @Override
    public void save(Customer customer) {
        //获取当前登录用户的员工
        Employee employee= UserContext.getUser();
        //设置销售员和录入人的员工id
        customer.setSeller(employee);
        customer.setInputUser(employee);
        //设置录入时间
        customer.setInputTime(new Date());
        customerMapper.insert(customer);
    }

    @Override
    public void delete(Long id) {
        customerMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(Customer customer) {
        customerMapper.updateByPrimaryKey(customer);
    }

    @Override
    public Customer get(Long id) {
        return customerMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Customer> listAll() {
        return customerMapper.selectAll();
    }

    @Override
    public PageInfo<Customer> query(CustomerQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageCount(), "c.input_time desc"); //对下一句sql进行自动分页,并按添加时间升序
        List<Customer> customers = customerMapper.selectForList(qo); //里面不需要自己写limit
        return new PageInfo<Customer>(customers);
    }

    @Override
    public Customer selectByTelId(String tel, Long id) {
        return customerMapper.selectByTelId(tel, id);
    }

    @Override
    public Customer selectByTel(String tel) {
        return customerMapper.selectByTel(tel);
    }

    @Override
    public void changeStatus(Long id, Integer status) {
        customerMapper.changeStatus(id, status);
    }
}
