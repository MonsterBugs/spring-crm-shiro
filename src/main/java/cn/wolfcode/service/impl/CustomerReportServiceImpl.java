package cn.wolfcode.service.impl;

import cn.wolfcode.mapper.CustomerReportMapper;
import cn.wolfcode.query.CustomerReportQuery;
import cn.wolfcode.service.ICustomerReportService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

@Service
@Transactional
public class CustomerReportServiceImpl implements ICustomerReportService {
    @Autowired
    private CustomerReportMapper customerReportMapper;
    @Override
    public PageInfo selectCustomerReport(CustomerReportQuery qo) {
        //设置当前页和每页数量
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageCount());
        //查询数据
        List<HashMap> list = customerReportMapper.selectCustomerReport(qo);
        return new PageInfo(list);
    }

    @Override
    public List<HashMap> selectAll(CustomerReportQuery qo) {
        return customerReportMapper.selectCustomerReport(qo);
    }
}
