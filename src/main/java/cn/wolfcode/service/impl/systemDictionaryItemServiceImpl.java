package cn.wolfcode.service.impl;

import cn.wolfcode.domain.SystemDictionaryItem;
import cn.wolfcode.mapper.systemDictionaryItemMapper;
import cn.wolfcode.query.SystemDictionaryItemQuery;
import cn.wolfcode.service.ISystemDictionaryItemService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class systemDictionaryItemServiceImpl implements ISystemDictionaryItemService {

    @Autowired
    private systemDictionaryItemMapper systemDictionaryItemMapper;


    @Override
    public void save(SystemDictionaryItem systemDictionaryItem) {
        //如果用户有没有填序号,查询指定parent_id目录最大序号.
        if(systemDictionaryItem.getSequence()==null){
            int maxSequence= systemDictionaryItemMapper.MaxSequenceByparent_id(systemDictionaryItem.getParentId());
            //设置用户的ParentId为maxSequence+1
            systemDictionaryItem.setSequence(maxSequence+1);
        }
        systemDictionaryItemMapper.insert(systemDictionaryItem);
    }

    @Override
    public void delete(Long id) {
        systemDictionaryItemMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(SystemDictionaryItem systemDictionaryItem) {
        systemDictionaryItemMapper.updateByPrimaryKey(systemDictionaryItem);
    }

    @Override
    public SystemDictionaryItem get(Long id) {
        return systemDictionaryItemMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<SystemDictionaryItem> listAll() {
        return systemDictionaryItemMapper.selectAll();
    }

    @Override
    public PageInfo<SystemDictionaryItem> query(SystemDictionaryItemQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageCount(),"sequence"); //对下一句sql进行自动分页和按sequence升序
        List<SystemDictionaryItem> systemDictionaryItems = systemDictionaryItemMapper.selectForList(qo); //里面不需要自己写limit
        return new PageInfo<SystemDictionaryItem>(systemDictionaryItems);
    }

    @Override
    public List<SystemDictionaryItem> selectByParentId(long parentId) {
        return systemDictionaryItemMapper.selectByParentId(parentId);
    }
}
