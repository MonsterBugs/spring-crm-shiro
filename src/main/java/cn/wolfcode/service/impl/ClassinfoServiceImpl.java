package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Classinfo;
import cn.wolfcode.mapper.ClassinfoMapper;
import cn.wolfcode.query.ClassinfoQuery;
import cn.wolfcode.service.IClassinfoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ClassinfoServiceImpl implements IClassinfoService {
    @Autowired
    private ClassinfoMapper classinfoMapper;

    @Override
    public void deleteByPrimaryKey(Long id) {
        classinfoMapper.deleteByPrimaryKey(id);

    }

    @Override
    public void insert(Classinfo classinfo) {
        classinfoMapper.insert(classinfo);
    }

    @Override
    public Classinfo selectByPrimaryKey(Long id) {
        return classinfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Classinfo> selectAll() {
        return classinfoMapper.selectAll();
    }

    @Override
    public void updateByPrimaryKey(Classinfo classinfo) {
        classinfoMapper.updateByPrimaryKey(classinfo);
    }

    @Override
    public PageInfo<Classinfo> queryForResult(ClassinfoQuery qo) {
        //设置当前页和每页条数
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageCount());
        List<Classinfo> list = classinfoMapper.list(qo);
        return new PageInfo(list);
    }
}
