package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Department;
import cn.wolfcode.mapper.DepartmentMapper;
import cn.wolfcode.query.Query;
import cn.wolfcode.service.IDepartmentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DepartmentServiceImpl implements IDepartmentService {
    @Autowired
    private DepartmentMapper departmentMapper;

    @Override
    public void deleteByPrimaryKey(Long id) {
        departmentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void insert(Department department) {
        departmentMapper.insert(department);
    }

    @Override
    public Department selectByPrimaryKey(Long id) {
        return departmentMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateByPrimaryKey(Department department) {
        departmentMapper.updateByPrimaryKey(department);
    }

    @Override
    public PageInfo<Department> queryForResult(Query qo) {
        //设置当前页和每页条数
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageCount());
        List<Department> list = departmentMapper.list(qo);
        return new PageInfo(list);
    }

    //全部
    public List<Department> listAll() {
        return departmentMapper.listAll();
    }
}

