package cn.wolfcode.service;

import cn.wolfcode.domain.Notice;
import cn.wolfcode.query.Query;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface INoticeService {
    void save(Notice notice);
    void delete(Long id);
    void update(Notice notice);
    Notice get(Long id);
    List<Notice> listAll();
    // 分页查询的方法
    PageInfo<Notice> query(Query qo);

    void read(Long noticeId);
}
