package cn.wolfcode.query;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CourseOrderQuery extends Query {
    private Long parentId;
    private Long courseId;

}
