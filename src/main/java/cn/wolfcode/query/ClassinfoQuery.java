package cn.wolfcode.query;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author: 谢佳宾
 * @Date： 2020/5/4
 * @Description: cn.wolfcode.query
 * @version: 1.0
 */
@Getter
@Setter
@ToString
public class ClassinfoQuery extends Query {
    private Integer keyId;
}
