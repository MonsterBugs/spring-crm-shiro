package cn.wolfcode.query;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author: 谢佳宾
 * @Date： 2020/5/4
 * @Description: cn.wolfcode.query
 * @version: 1.0
 */
@Getter
@Setter
@ToString
public class CustomerQuery extends Query {
    //客户的状态
    private Integer status;
    //销售员id
    private Long sellerId=-1L;
}
