package cn.wolfcode.query;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class JsonResult {
    private boolean success=true;
    private  String msg="操作成功";
}
