package cn.wolfcode.query;

import cn.wolfcode.unit.DateUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@ToString
@Setter
@Getter
public class CustomerReportQuery extends Query {
    //开始时间("yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    //结束时间("yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    //客户类型
    private Integer customerStatus;
    //分组类型
    private String groupType = "e.name";//默认按照员工姓名分组
    //业务逻辑要求endTime需要当日最晚时间23:59:59
    public Date getEndTime(){
        return DateUtil.getEndDate(endTime);
    }

}
