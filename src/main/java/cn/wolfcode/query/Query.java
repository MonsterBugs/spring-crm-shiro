package cn.wolfcode.query;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

//本类为了将用户传入的currentpage和pagesize封装成对象,供业务层调用
@Getter
@Setter
@ToString
public class Query {
    private String keyword;//关键字
    private int currentPage = 1;// 当前页
    private int pageCount = 2;// 每页条数
}