package cn.wolfcode.query;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: 谢佳宾
 * @Date： 2020/5/4
 * @Description: cn.wolfcode.query
 * @version: 1.0
 */
@Getter
@Setter
public class EmployeeQuery extends Query {
    private Integer deptId=-1;

}
