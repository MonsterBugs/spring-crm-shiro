package cn.wolfcode.query;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SystemDictionaryItemQuery extends Query {
    //目录id
    private Long parentId = 1L;

}
