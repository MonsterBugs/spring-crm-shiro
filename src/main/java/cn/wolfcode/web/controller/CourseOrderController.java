package cn.wolfcode.web.controller;


import cn.wolfcode.domain.CourseOrder;
import cn.wolfcode.query.CourseOrderQuery;
import cn.wolfcode.service.ICourseOrderService;
import cn.wolfcode.query.Query;
import cn.wolfcode.query.JsonResult;
import cn.wolfcode.service.ICustomerService;
import cn.wolfcode.service.IEmployeeService;
import cn.wolfcode.service.ISystemDictionaryItemService;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/courseOrder")
public class CourseOrderController {

    @Autowired
    private ICourseOrderService courseOrderService;
    @Autowired
    private ICustomerService customerService;
    @Autowired
    private ISystemDictionaryItemService systemDictionaryItemService;

    @RequiresPermissions(value = {"courseOrder:list","课程页面"},logical = Logical.OR)
    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo") CourseOrderQuery qo){
        //设置查询条件(prarentId)
        qo.setParentId(4L);
        PageInfo<CourseOrder> pageInfo = courseOrderService.query(qo);
        //共享数据给页面列表
        model.addAttribute("pageResult", pageInfo);
        //共享数据给模态框的客户姓名
        model.addAttribute("coustomers",customerService.listAll());
        //共享数据给模态框的课程
        model.addAttribute("courses",systemDictionaryItemService.selectByParentId(4L));
        return "courseOrder/list";
    }

    @RequestMapping("/delete")
    @RequiresPermissions(value = {"courseOrder:delete","课程删除"},logical = Logical.OR)
    @ResponseBody
    public JsonResult delete(Long id){
        if (id != null) {
            courseOrderService.delete(id);
        }
        return new JsonResult();
    }


    @RequestMapping("/saveOrUpdate")
    @RequiresPermissions(value = {"courseOrder:saveOrUpdate","课程新增/编辑"},logical = Logical.OR)
    @ResponseBody
    public JsonResult saveOrUpdate(CourseOrder courseOrder){
        if (courseOrder.getId() != null) {
            courseOrderService.update(courseOrder);
        }else {
            courseOrderService.save(courseOrder);
        }
        return new JsonResult();
    }
}
