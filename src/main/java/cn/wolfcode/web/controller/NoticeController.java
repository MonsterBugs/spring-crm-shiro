package cn.wolfcode.web.controller;


import cn.wolfcode.domain.Notice;
import cn.wolfcode.domain.Notice_Employee;
import cn.wolfcode.mapper.Notice_EmployeeMapper;
import cn.wolfcode.service.INoticeService;
import cn.wolfcode.query.Query;
import cn.wolfcode.query.JsonResult;
import cn.wolfcode.unit.UserContext;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/notice")
public class NoticeController {

    @Autowired
    private INoticeService noticeService;
    @Autowired
    private Notice_EmployeeMapper ne;

    @RequiresPermissions(value = {"notice:list", "公告页面"}, logical = Logical.OR)
    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo") Query qo) {
        PageInfo<Notice> pageInfo = noticeService.query(qo);
        model.addAttribute("pageResult", pageInfo);
        return "notice/list";
    }

    @RequestMapping("/delete")
    @RequiresPermissions(value = {"notice:delete", "公告删除"}, logical = Logical.OR)
    @ResponseBody
    public JsonResult delete(Long id) {
        if (id != null) {
            noticeService.delete(id);
        }
        return new JsonResult();
    }


    @RequestMapping("/saveOrUpdate")
    @RequiresPermissions(value = {"notice:saveOrUpdate", "公告新增/编辑"}, logical = Logical.OR)
    @ResponseBody
    public JsonResult saveOrUpdate(Notice notice) {
        if (notice.getId() != null) {
            noticeService.update(notice);
        } else {
            noticeService.save(notice);
        }
        return new JsonResult();
    }

    @RequestMapping("/get") //公告查看栏
    public String get(Model model,Long id) {
        model.addAttribute("notice",noticeService.get(id));
        return "notice/show";
    }

    @RequestMapping("/read") //公告已阅
    public  void read(Long noticeId){
        //判断用户是否已读
        Notice_Employee notice_employee = ne.selectByEmployeeIdNoticeId(UserContext.getUser().getId(), noticeId);
        if(notice_employee!=null){
            return;
        }
        noticeService.read(noticeId);
        return;
    }
}
