package cn.wolfcode.web.controller;

import cn.wolfcode.query.JsonResult;
import cn.wolfcode.query.Query;
import cn.wolfcode.service.IPermissionService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller     //生成permissionController
@RequestMapping("/permission")
public class PermssionController {
    @Autowired
    private IPermissionService service;

    //查询角色分页数据-
    @RequiresPermissions(value = {"permission:list","权限列表"},logical = Logical.OR)
    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo") Query qo) {
        model.addAttribute("pageResult", service.queryForResult(qo));
        return "permission/list";
    }

    //删除
    @RequiresPermissions(value = {"permission:delete","权限删除"},logical = Logical.OR)
    @RequestMapping("/delete")
    @ResponseBody
    public JsonResult delete(Model mode, Long id) {
        if (id != null) {
            service.deleteByPrimaryKey(id);
        }
        return new JsonResult(true, "删除成功");
    }

    //重新加载
    @RequiresPermissions(value = {"permission:reload","权限重新加载"},logical = Logical.OR)
    @RequestMapping("/reload")
    @ResponseBody
    public JsonResult reload(Model mode, Long id) {
        service.reload();
        return new JsonResult(true, "加载成功");
    }
}