package cn.wolfcode.web.controller;

import cn.wolfcode.query.CustomerReportQuery;
import cn.wolfcode.service.ICustomerReportService;
import cn.wolfcode.unit.MessageUtil;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import org.omg.CORBA.DATA_CONVERSION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/customerReport")
public class CustomerReportController {
    @Autowired
    private ICustomerReportService service;
    //普通列表
    @RequestMapping("/list")
    public String customerReport(Model model,@ModelAttribute("qo") CustomerReportQuery qo){
        //只查询潜在客户
        qo.setCustomerStatus(0);
        //共享数据到列表页面
        model.addAttribute("pageResult",service.selectCustomerReport(qo));
        //跳转视图
        return "customerReport/list";
    }

    //柱状图
    @RequestMapping("/bar")
    public String bar(Model model,@ModelAttribute("qo") CustomerReportQuery qo){
        //只查询潜在客户
        qo.setCustomerStatus(0);
        //查询数据
        List<HashMap> data=service.selectAll(qo);
        //创建两个数组
        ArrayList<Object> xList = new ArrayList<>();
        ArrayList<Object> yList = new ArrayList<>();
        for (HashMap map : data) {
            xList.add(map.get("groupType"));
            yList.add(map.get("number"));
        }
        //freemake不能直接获取集合,只能遍历,故对xList yList 进行json或toString处理;
        model.addAttribute("xList", JSON.toJSONString(xList));
        model.addAttribute("yList", JSON.toJSONString(yList));
        //分组类型转为正常文字显示
        model.addAttribute("groupType", MessageUtil.changMsg(qo));
        //跳转视图
        return "customerReport/bar";
    }

    //柱状图
    @RequestMapping("/pie")
    public String pie(Model model,@ModelAttribute("qo") CustomerReportQuery qo){
        //只查询潜在客户
        qo.setCustomerStatus(0);
        //获取所有数据,不分页
        List<HashMap> list = service.selectAll(qo);
        //准备第第一个集合,用于储存groupType数据    ["admin","钟总"]
        ArrayList<Object> groupTypeValues = new ArrayList<>();
        //准备第二个数组,用于储存饼状图需要的类型数据数据    [{value: 25, name: 'admin'},{value: 50, name: '张总'}]
        ArrayList<Object> data = new ArrayList<>();
        for (HashMap map : list) {
            groupTypeValues.add(map.get("groupType"));
            HashMap<Object, Object> hashMap = new HashMap<>();
            hashMap.put("name",map.get("groupType")); //name: 'admin'
            hashMap.put("value",map.get("number"));     //value: 25
            data.add(hashMap);                      //{value: 25, name: 'admin'}
        }
        //共享数据(freemarke不能直接获取集合,故对结合进行json化处理)
        model.addAttribute("groupTypeValues",JSON.toJSONString(groupTypeValues));
        model.addAttribute("data",JSON.toJSONString(data));
        model.addAttribute("groupType", MessageUtil.changMsg(qo));
        return "customerReport/pie";
    }
}
