package cn.wolfcode.web.controller;


import cn.wolfcode.domain.SystemDictionaryItem;
import cn.wolfcode.query.SystemDictionaryItemQuery;
import cn.wolfcode.service.ISystemDictionaryItemService;
import cn.wolfcode.query.JsonResult;
import cn.wolfcode.service.ISystemDictionaryService;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/systemDictionaryItem")
public class systemDictionaryItemController {

    @Autowired
    private ISystemDictionaryItemService systemDictionaryItemService;
    @Autowired
    private ISystemDictionaryService systemDictionaryService;


    @RequiresPermissions(value = {"systemDictionaryItem:list","权限页面"},logical = Logical.OR)
    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo") SystemDictionaryItemQuery qo){
        PageInfo<SystemDictionaryItem> pageInfo = systemDictionaryItemService.query(qo);
        model.addAttribute("pageResult", pageInfo);
        model.addAttribute(("systemDictionarys"),systemDictionaryService.listAll());
        return "systemDictionaryItem/list";
    }

    @RequestMapping("/delete")
    @RequiresPermissions(value = {"systemDictionaryItem:delete","权限删除"},logical = Logical.OR)
    @ResponseBody
    public JsonResult delete(Long id){
        if (id != null) {
            systemDictionaryItemService.delete(id);
        }
        return new JsonResult();
    }


    @RequestMapping("/saveOrUpdate")
    @RequiresPermissions(value = {"systemDictionaryItem:saveOrUpdate","权限新增/编辑"},logical = Logical.OR)
    @ResponseBody
    public JsonResult saveOrUpdate(SystemDictionaryItem systemDictionaryItem){
        if (systemDictionaryItem.getId() != null) {
            systemDictionaryItemService.update(systemDictionaryItem);
        }else {
            systemDictionaryItemService.save(systemDictionaryItem);
        }
        return new JsonResult(true,"操作成功");
    }
}
