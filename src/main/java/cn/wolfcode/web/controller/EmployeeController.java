package cn.wolfcode.web.controller;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.query.EmployeeQuery;
import cn.wolfcode.query.JsonResult;
import cn.wolfcode.service.IDepartmentService;
import cn.wolfcode.service.IEmployeeService;
import cn.wolfcode.service.IRoleService;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@Controller     //生成employeeController
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private IEmployeeService employeeService;
    @Autowired
    private IDepartmentService departmentService;
    @Autowired
    private IRoleService roleService;

    //查询员工分页数据-
    @RequestMapping("/list")
    @RequiresPermissions(value = {"employee:list", "员工列表"}, logical = Logical.OR)
    public String list(Model model, @ModelAttribute("qo") EmployeeQuery qo) {
        //员工列表数据
        model.addAttribute("pageResult", employeeService.queryForResult(qo));
        //部门下拉框数据
        model.addAttribute("depts", departmentService.listAll());
        return "employee/list";
    }

    //跳转到input.jsp页面
    @RequestMapping("/input")
    public String input(Model model, Long id) {
        //如果请求参数带了id,则为修改操作,则需要数据回显给input页面
        if (id != null) {
            model.addAttribute("employee", employeeService.selectByPrimaryKey(id));
        }
        model.addAttribute("roles", roleService.listAll());
        model.addAttribute("depts", departmentService.listAll());
        return "employee/input";
    }

    //修改或添加新的员工
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    @RequiresPermissions(value = {"employee:saveOrUpdate", "员工添加/编辑"}, logical = Logical.OR)
    public JsonResult saveOrUpdate(Employee employee, long[] ids) {
        if (employee.getId() != null) {
            employeeService.updateByPrimaryKey(employee, ids);
            return new JsonResult(true, "操作成功");
        } else {
            employeeService.insert(employee, ids);
            return new JsonResult(true, "操作成功");
        }
    }

    //删除
    @RequestMapping("/delete")
    @RequiresPermissions(value = {"employee:delete", "员工删除"}, logical = Logical.OR)
    @ResponseBody
    public JsonResult delete(Model mode, Long id) {
        if (id != null) {
            employeeService.deleteByPrimaryKey(id);
        }
        return new JsonResult(true, "操作成功");
    }

    @RequestMapping("/updatePwdInput")
    public String updatePwdInput(Model model, Long id) {
        //model.addAttribute("id", id); //
        return "employee/updatePwd";
    }

    @RequestMapping("/updatePwd")
    @ResponseBody
    @RequiresPermissions(value = {"employee:updatePwd", "员工密码更新"}, logical = Logical.OR)
    public JsonResult updatePwd(String oldPassword, String newPassword) {
        return employeeService.updatePwd(oldPassword, newPassword);

    }

    @RequestMapping("/restpwdInput")
    public String restpwdInput(Model model, Long id) {
        model.addAttribute("employee", employeeService.selectByPrimaryKey(id));
        return "employee/resetPwd";
    }

    @ResponseBody
    @RequestMapping("/resetPwd")
    @RequiresPermissions(value = {"employee:resetPwd", "员工密码重置"}, logical = Logical.OR)
    public JsonResult resetPwd(Long id, String newPassword, String name) {
        employeeService.restPwd(name, newPassword);
        return new JsonResult(true, "重置密码成功");
    }

    @ResponseBody
    @RequestMapping("/batchDelete")
    @RequiresPermissions(value = {"employee:batchDelete", "员工批量删除"}, logical = Logical.OR)
    public JsonResult batchDelete(Long[] ids) {
        employeeService.batchDelete(ids);
        return new JsonResult(true, "操作成功");
    }

    @RequestMapping("/checkName")
    @ResponseBody
    public Map<String, Boolean> checkName(String name, Long id) {
        HashMap<String, Boolean> map = new HashMap<>();
        if (id != null) {
            Employee employee = employeeService.selectByNameId(name, id);
            //更新员工信息未修改过员工名
            if (employee != null) {
                map.put("valid", true);
                return map;
            }
        }
        //判断数据库是否有同名员工
        Employee employee = employeeService.selectByName(name);
        map.put("valid", employee == null);
        return map;

    }

    @RequestMapping("/changeStatus")
    @ResponseBody
    @RequiresPermissions(value = {"employee:changeStatus", "员工状态更改"}, logical = Logical.OR)
    public JsonResult changeStatus(Long id, boolean status) {
        employeeService.changeStatus(id, !status);
        return new JsonResult(true, "操作成功");
    }

    //员工数据导出
    @RequestMapping("/exportXls")
    @RequiresPermissions(value = {"employee:exportXls", "员工数据导出"}, logical = Logical.OR)
    public void exportXls(HttpServletResponse response) throws IOException {
        //文件下载的响应头
        response.setHeader("Content-Disposition", "attachment;filename=employee.xls");
        //调用业务方法,查询员工数据,封装到excel文件中
        Workbook workbook = employeeService.exportXls();
        //板excel数据输出给浏览器
        workbook.write(response.getOutputStream());
    }

    //员工数据导入
    @RequestMapping("/importXls")
    @RequiresPermissions(value = {"employee:importXls", "员工数据导入"}, logical = Logical.OR)
    @ResponseBody
    public JsonResult exportXls(MultipartFile file) throws IOException {
        //调用业务方法,读取员工数据,导入数据库
        employeeService.importXls(file);
        return new JsonResult(true, "数据导入成功");
    }
}
