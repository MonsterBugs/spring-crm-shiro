package cn.wolfcode.web.controller;

import cn.wolfcode.domain.Department;
import cn.wolfcode.query.JsonResult;
import cn.wolfcode.query.Query;
import cn.wolfcode.service.IDepartmentService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller     //生成departmentController
@RequestMapping("/department")
public class DepartmentController {
    @Autowired
    private IDepartmentService service;

    //查询团队分页数据-
    @RequestMapping("/list")
    @RequiresPermissions(value = {"department:list","部门列表"},logical = Logical.OR)
    public String list(Model model, @ModelAttribute("qo") Query qo) {
        model.addAttribute("pageResult", service.queryForResult(qo));
        return "department/list";
    }


    //修改或添加新的团队
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    @RequiresPermissions(value = {"department:saveOrUpdate","部门添加/更新"},logical = Logical.OR)
    public JsonResult saveOrUpdate(Department department) {
        if (department.getId() != null) {
            service.updateByPrimaryKey(department);
            return new JsonResult(true,"操作成功");
        } else {
            service.insert(department);
            return new JsonResult(true,"操作成功");
        }
    }

    //删除
    @RequestMapping("/delete")
    @ResponseBody
    @RequiresPermissions(value = {"department:delete","部门删除"},logical = Logical.OR)
    public JsonResult delete(Long id) {
        try {
           service.deleteByPrimaryKey(id);
            return new JsonResult(true,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new JsonResult(false,"操作失败");
        }
    }

}
