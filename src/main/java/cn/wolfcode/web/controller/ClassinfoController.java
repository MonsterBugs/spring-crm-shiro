package cn.wolfcode.web.controller;

import cn.wolfcode.domain.Classinfo;
import cn.wolfcode.query.ClassinfoQuery;
import cn.wolfcode.query.JsonResult;
import cn.wolfcode.service.IClassinfoService;
import cn.wolfcode.service.IEmployeeService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller     //生成classinfoController
@RequestMapping("/classinfo")
public class ClassinfoController {
    @Autowired
    private IClassinfoService service;
    @Autowired
    private IEmployeeService employeeService;

    //查询团队分页数据-
    @RequestMapping("/list")
    @RequiresPermissions(value = {"classinfo:list","班级列表"},logical = Logical.OR)
    public String list(Model model, @ModelAttribute("qo") ClassinfoQuery qo) {
        model.addAttribute("pageResult", service.queryForResult(qo));
        model.addAttribute("employees", employeeService.selectAll());
        return "classinfo/list";
    }


    //修改或添加新的团队
    @RequestMapping("/saveOrUpdate")
    @RequiresPermissions(value = {"classinfo:saveOrUpdate","班级添加/编辑"},logical = Logical.OR)
    @ResponseBody
    public JsonResult saveOrUpdate(Classinfo classinfo) {
        if (classinfo.getId() != null) {
            service.updateByPrimaryKey(classinfo);
        } else {
            service.insert(classinfo);
        }
        return new JsonResult(true,"操作成功");
    }

    //删除
    @RequestMapping("/delete")
    @ResponseBody
    @RequiresPermissions(value = {"classinfo:delete","班级删除"},logical = Logical.OR)
    public JsonResult delete(Long id) {
           service.deleteByPrimaryKey(id);
            return new JsonResult(true,"操作成功");
    }

}
