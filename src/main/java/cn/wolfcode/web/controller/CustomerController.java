package cn.wolfcode.web.controller;


import cn.wolfcode.domain.Customer;
import cn.wolfcode.domain.Employee;
import cn.wolfcode.query.CustomerQuery;
import cn.wolfcode.service.ICustomerService;
import cn.wolfcode.query.JsonResult;
import cn.wolfcode.service.IEmployeeService;
import cn.wolfcode.service.ISystemDictionaryItemService;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private ICustomerService customerService;
    @Autowired
    private IEmployeeService employeeService;
    @Autowired
    private ISystemDictionaryItemService systemDictionaryItemService;

    @RequiresPermissions(value = {"customer:potentialList", "潜在客户页面"}, logical = Logical.OR)
    @RequestMapping("/potentialList")
    public String potentialList(Model model, @ModelAttribute("qo") CustomerQuery qo) {
        qo.setStatus(Customer.STATUS_COMMON);   //只查询潜在客户
        //获取当前登录主体
        Subject subject = SecurityUtils.getSubject();
        //非管理员和市场经理,只能看到自己的客户
        if (!(subject.hasRole("ADMIN")||subject.hasRole("Market_Manager"))) {
            //获取登录用户的id
            Employee employee = (Employee) subject.getPrincipal();
            //根据销售人员id查询客户
            qo.setSellerId(employee.getId());
        }
        //根据qo条件查询客户,并共享客户列表中
        PageInfo<Customer> pageInfo = customerService.query(qo);
        model.addAttribute("pageResult", pageInfo);
        //Market Market_Manager,并共享到高级查询下拉框
        List<Employee> employees = employeeService.selectByRoleSn("Market", "Market_Manager");
        model.addAttribute("sellers",employees);
        //共享所有客户的工作和客户来源,供客户添加/编辑模态框的下拉框使用
        model.addAttribute("jobs",systemDictionaryItemService.selectByParentId(1L));
        model.addAttribute("sources",systemDictionaryItemService.selectByParentId(2L));
        //共享所有联系方式，共跟进模态框使用
        model.addAttribute("ccts",systemDictionaryItemService.selectByParentId(26L));
        return "customer/potentialList";
    }


    @RequiresPermissions(value = {"customer:poolList", "客户池页面"}, logical = Logical.OR)
    @RequestMapping("/poolList")
    public String poolList(Model model, @ModelAttribute("qo") CustomerQuery qo) {
        qo.setStatus(Customer.STATUS_POOL);   //只查询客户池客户
        //根据qo条件查询客户,并共享客户列表中
        PageInfo<Customer> pageInfo = customerService.query(qo);
        model.addAttribute("pageResult", pageInfo);
        //根据角色编码查询拥有该角色的员工 和Market Market_Manager,并共享到高级查询下拉框
        List<Employee> employees = employeeService.selectByRoleSn("Market", "Market_Manager");
        model.addAttribute("employees",employees);
        return "customer/poolList";
    }

    @RequiresPermissions(value = {"customer:failList", "失败客户页面"}, logical = Logical.OR)
    @RequestMapping("/failList")
    public String failList(Model model, @ModelAttribute("qo") CustomerQuery qo) {
        qo.setStatus(Customer.STATUS_FAIL);   //只查询客户池客户
        //根据qo条件查询客户,并共享客户列表中
        PageInfo<Customer> pageInfo = customerService.query(qo);
        model.addAttribute("pageResult", pageInfo);
        //根据角色编码查询拥有该角色的员工 和Market Market_Manager,并共享到高级查询下拉框
        List<Employee> employees = employeeService.selectByRoleSn("Market", "Market_Manager");
        model.addAttribute("employees",employees);
        return "customer/failList";
    }

    @RequiresPermissions(value = {"customer:normalList", "失败客户页面"}, logical = Logical.OR)
    @RequestMapping("/normalList")
    public String normalList(Model model, @ModelAttribute("qo") CustomerQuery qo) {
        qo.setStatus(Customer.STATUS_NORMAL);   //只查询客户池客户
        //根据qo条件查询客户,并共享客户列表中
        PageInfo<Customer> pageInfo = customerService.query(qo);
        model.addAttribute("pageResult", pageInfo);
        //根据角色编码查询拥有该角色的员工 和Market Market_Manager,并共享到高级查询下拉框
        List<Employee> employees = employeeService.selectByRoleSn("Market", "Market_Manager");
        model.addAttribute("employees",employees);
        return "customer/normalList";
    }

    @RequiresPermissions(value = {"customer:lostList", "失败客户页面"}, logical = Logical.OR)
    @RequestMapping("/lostList")
    public String loselList(Model model, @ModelAttribute("qo") CustomerQuery qo) {
        qo.setStatus(Customer.STATUS_LOST);   //只查询客户池客户
        //根据qo条件查询客户,并共享客户列表中
        PageInfo<Customer> pageInfo = customerService.query(qo);
        model.addAttribute("pageResult", pageInfo);
        //根据角色编码查询拥有该角色的员工 和Market Market_Manager,并共享到高级查询下拉框
        List<Employee> employees = employeeService.selectByRoleSn("Market", "Market_Manager");
        model.addAttribute("employees",employees);
        return "customer/lostList";
    }

    @RequiresPermissions(value = {"customer:List", "客户池列表"}, logical = Logical.OR)
    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo") CustomerQuery qo) {
        PageInfo<Customer> pageInfo = customerService.query(qo);
        model.addAttribute("pageResult", pageInfo);
        //根据角色编码查询拥有该角色的员工 和Market Market_Manager,并共享到高级查询下拉框
        List<Employee> employees = employeeService.selectByRoleSn("Market", "Market_Manager");
        model.addAttribute("employees",employees);
        return "customer/list";
    }

    @RequestMapping("/delete")
    @RequiresPermissions(value = {"customer:delete", "客户删除"}, logical = Logical.OR)
    @ResponseBody
    public JsonResult delete(Long id) {
        if (id != null) {
            customerService.delete(id);
        }
        return new JsonResult();
    }


    @RequestMapping("/saveOrUpdate")
    @RequiresPermissions(value = {"customer:saveOrUpdate", "客户新增/编辑"}, logical = Logical.OR)
    @ResponseBody
    public JsonResult saveOrUpdate(Customer customer) {
        if (customer.getId() != null) {
            customerService.update(customer);
        } else {
            customerService.save(customer);
        }
        return new JsonResult(true,"操作成功");
    }

    @RequestMapping("/checkTel")
    @ResponseBody
    public Map<String, Boolean> checkName(String tel, Long id) {
        HashMap<String, Boolean> map = new HashMap<>();
        //更新客户信息,是否修改过客户电话
        if (id != null) {
            Customer customer = customerService.selectByTelId(tel, id);
            if (customer != null) {
                map.put("valid", true);
                return map;
            }
        }
        //添加新客户,判断电话号码是否被使用
        Customer customer = customerService.selectByTel(tel);
        map.put("valid", customer == null);
        return map;
    }

    @RequestMapping("/changeStatus")
    @RequiresPermissions(value = {"customer:saveOrUpdate", "修改客户状态"}, logical = Logical.OR)
    @ResponseBody
    public JsonResult changeStatus(Long id,Integer status) {
        customerService.changeStatus(id,status);
        return new JsonResult(true,"操作成功");
    }
}
