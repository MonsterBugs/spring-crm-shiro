package cn.wolfcode.web.controller;

import cn.wolfcode.domain.Role;
import cn.wolfcode.query.JsonResult;
import cn.wolfcode.query.Query;
import cn.wolfcode.service.IPermissionService;
import cn.wolfcode.service.IRoleService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller     //生成roleController
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private IRoleService service;
    @Autowired
    private IPermissionService permissionService;

    //查询团队分页数据-
    @RequestMapping("/list")
    @RequiresPermissions(value = {"role:list","角色列表"},logical = Logical.OR)
    public String list(Model model, @ModelAttribute("qo") Query qo) {
        model.addAttribute("pageResult", service.queryForResult(qo));
        return "role/list";
    }

    //跳转到input.jsp页面
    @RequestMapping("/input")
    public String input(Model model, Long id) {
        //如果请求参数带了id,则为修改操作,则需要数据回显给input页面
        if (id != null) {
            model.addAttribute("role", service.selectByPrimaryKey(id));//回显角色信息
        }
        model.addAttribute("permissions", permissionService.listAll());
        return "role/input";
    }

    //修改或添加新的团队
    @RequestMapping("/saveOrUpdate")
    @RequiresPermissions(value = {"role:saveOrUpdate","角色添加/编辑"},logical = Logical.OR)
    @ResponseBody
    public JsonResult saveOrUpdate(Role role, long[] ids) {
        if (role.getId() != null) {
            service.updateByPrimaryKey(role, ids);
        } else {
            service.insert(role, ids);
        }
         return new JsonResult(true, "删除成功");
    }

    //删除
    @RequestMapping("/delete")
    @RequiresPermissions(value = {"role:delete","角色删除"},logical = Logical.OR)
    @ResponseBody
    public JsonResult delete(Model mode, Long id) {
        if (id != null) {
            service.deleteByPrimaryKey(id);
        }
        return new JsonResult(true, "删除成功");
    }

}
