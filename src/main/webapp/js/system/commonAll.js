$(function () {
    $.messager.model = {
        ok:{text:"确认"},
        cancel:{text:"取消"}
    }

})

var handlerMessage = function (data) { //form中已经设置了action,method
    if(data.success){
        $.messager.alert("温馨提示","操作成功!1s后自动关闭")
        window.setTimeout(function () {
            window.location.reload();
        },1000)
    }else{
        $.messager.popup(data.msg)
    }
}
//禁用数组添加[]的功能
jQuery.ajaxSettings.traditional = true;
