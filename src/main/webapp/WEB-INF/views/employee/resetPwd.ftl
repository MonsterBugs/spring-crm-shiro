<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>重置密码</title>
    <#include "../common/link.ftl">
    <!--菜单回显-->
    <#assign currentMenu="employee"/>
    <script>
        $(function () {
            $('#submitBtn').click(function () {
                //serialize方法,获取表单所有name对应的value,并拼接name1=value1&name2=value2.......提交后台
                $.post('/employee/resetPwd.do', $('#editForm').serialize(), function (data) {
                    //判断是否修改成功,成功跳转到员工列表页面
                    if (data.success) {
                        location.href = '/employee/list.do';
                    } else {
                        $.messager.popup.alert(data.msg)
                    }
                })
            })
        })
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="employee"/>
    <#include "../common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>重置密码</h1>
        </section>
        <section class="content">
            <div class="box" style="padding: 30px;">
                <form class="form-horizontal" method="post" id="editForm"action="">
                    <input type="hidden"  name="id" value="${employee.id}">
                    <input type="hidden"  name="name" value="${employee.name}">
                    <div class="form-group" style="text-align: center;">
                        <h3>您正在重置员工${employee.name}的密码</h3>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="sn" class="col-sm-3 control-label">新密码：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="newPassword"
                                   placeholder="请输入新密码">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button id="submitBtn" type="button" class="btn btn-primary">确定重置</button>
                        </div>
                    </div>
                </form>

            </div>
        </section>
    </div>
    <#include "../common/footer.ftl">
</div>
</body>
</html>
