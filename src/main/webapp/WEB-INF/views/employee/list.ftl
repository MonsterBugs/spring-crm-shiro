<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>员工管理</title>
    <#include "../common/link.ftl">
    <script>
        $(function () {
            //删除
            $('.btn_delete').click(function () {
                var json = $(this).data('json');
                $.get('/employee/delete.do', json, handlerMessage)
            })

            //员工禁用和恢复
            $('.btn_tf').click(function () {
                //获取json对象
                var json = $(this).data('json');
                //为了改变员工状态,还在当前页,选择发送ajax请求
                $.get('/employee/changeStatus.do', json, function (data) {
                    if (data.success) {
                        location.reload();
                    } else {
                        $.messager.popup(data.msg)
                    }
                })


            })
            //当用户点击或取消全选按钮,列表框的全部复选框为选中或不选中
            $('.allCb').click(function () {
                $(".cb").prop('checked', $(this).prop("checked"))
            })
            //监听用户是否全部选中
            $(".cb").click(function () {
                $(".allCb").prop("checked", $(".cb:checked").length == $(".cb").length)
            })

            //绑定批量删除按钮
            $('.btn-batchDelete').click(function () {
                //获取用户选中的数据
                var cbs = $(".cb:checked");
                //判断用户是否有选中数据
                if (cbs.length == 0) {
                    $.messager.popup("您没有选中数据")
                }
                //获取用户选中的用户id数组
                var ids = [];
                $.each(cbs, function (i, ele) {
                    ids.push($(ele).data('id'));
                })
                console.log(ids);
                //确认框
                $.messager.confirm('警告!!', '请确认是否删除', function () { //点击确定后的回调函数
                    //发送异步请求
                    $.post('/employee/batchDelete.do', {ids: ids}, handlerMessage)
                })

            })
            //触发数据导入模态框
            $(".btn-import").click(function () {
                $("#importModal").modal('show');
            })

            //保存模态框数据
            $(".btn-submit").click(function () {
                $("#importForm").ajaxSubmit(handlerMessage);
            })

        })
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="employee"/>
    <#include "../common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>员工管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <div style="margin: 10px;">
                    <form class="form-inline" id="searchForm" action="/employee/list.do" method="post">
                        <input type="hidden" name="currentPage" id="currentPage" value="1">
                        <div class="form-group">
                            <label for="keyword">关键字:</label>
                            <input type="text" class="form-control" id="keyword" name="keyword" value="${qo.keyword!}"
                                   placeholder="请输入姓名/邮箱">
                        </div>
                        <div class="form-group">
                            <label for="dept"> 部门:</label>
                            <select class="form-control" id="dept" name="deptId">
                                <option value="-1">全部</option>
                                <#list depts as d>
                                    <option value="${d.id}">${d.name!}</option>
                                </#list>
                            </select>
                            <script>
                                $("#dept").val(${qo.deptId!});
                            </script>
                        </div>
                        <button id="btn_query" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>
                            查询
                        </button>
                        <@shiro.hasPermission name="employee:saveOrUpdate">
                        <a href="/employee/input.do" class="btn btn-success btn_redirect">
                            <span class="glyphicon glyphicon-plus"></span> 添加
                        </a>
                        </@shiro.hasPermission>
                        <@shiro.hasPermission name="employee:batchDelete">
                        <a href="#" class="btn btn-danger btn-batchDelete">
                            <span class="glyphicon glyphicon-trash"></span> 批量删除
                        </a>
                        </@shiro.hasPermission>
                        <a href="/employee/exportXls.do" class="btn btn-warning">
                            <span class="glyphicon glyphicon-download"></span> 导出
                        </a>
                        <a href="#" class="btn btn-warning btn-import">
                            <span class="glyphicon glyphicon-upload"></span> 导入
                        </a>
                </div>
                </form>
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th><input type="checkbox" class="allCb"></th>
                        <th>编号</th>
                        <th>名称</th>
                        <th>email</th>
                        <th>年龄</th>
                        <th>部门</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <#list pageResult.list as employee>
                        <tr>
                            <td><input type="checkbox" class="cb" data-id="${employee.id}"></td>
                            <td>${employee_index+1}</td>
                            <td>${employee.name!}</td>
                            <td>${employee.email!}</td>
                            <td>${employee.age!}</td>
                            <td>${(employee.dept.name)!}</td>
                            <td>${employee.status?string("正常","禁用")}</td>
                            <td>
                        <@shiro.hasPermission name="employee:saveOrUpdate">
                                <a href="/employee/input.do?id=${employee.id}" class="btn btn-info btn-xs btn_redirect">
                                    <span class="glyphicon glyphicon-pencil"></span> 编辑
                                </a>
                        </@shiro.hasPermission>
                        <@shiro.hasPermission name="employee:delete">
                                <a href="#" data-json='${employee.json}'
                                   class="btn btn-danger btn-xs btn_delete">
                                    <span class="glyphicon glyphicon-trash"></span> 删除
                                </a>
                        </@shiro.hasPermission>
                            <#--正常-->
                        <@shiro.hasPermission name="employee:changeStatus">
                            <#if employee.status>
                    <a href="#" data-json='${employee.json}'
                       class="btn btn-warning btn-xs btn_tf">
                        <span class="glyphicon glyphicon-tags"></span> 禁用
                    </a>
                            </#if>
                        <#--禁用-->
                            <#if !employee.status>
                    <a href="#" data-json='${employee.json}'
                       class="btn btn-success btn-xs btn_tf">
                        <span class="glyphicon glyphicon-tags"></span> 恢复
                    </a>
                            </#if>
                        </@shiro.hasPermission>
                <@shiro.hasPermission name="employee:resetPwd">
                    <a class="btn btn-danger btn-xs btn_restPwd" id="restPwd"
                       href="/employee/restpwdInput.do?id=${employee.id}">
                        <span class="glyphicon glyphicon-trash"></span> 重置密码
                    </a>
                </@shiro.hasPermission>
                            </td>
                        </tr>
                    </#list>
                </table>
                <!--分页-->
            <#include "../common/page.ftl">
            </div>
        </section>
    </div>
<#include "../common/footer.ftl">
</div>
</body>
<#--数据导入模态框-->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">导入</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/employee/importXls.do" method="post" id="importForm"
                      enctype="multipart/form-data">
                    <input type="hidden" name="id">
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label"></label>
                        <div class="col-sm-6">
                            <input type="file" name="file">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                            <a href="/xlstemplates/employee_import.xls" class="btn btn-success">
                                <span class="glyphicon glyphicon-download"></span> 下载模板
                            </a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="submit" class="btn btn-primary btn-submit">保存</button>
            </div>
        </div>
    </div>
</div>
</html>
