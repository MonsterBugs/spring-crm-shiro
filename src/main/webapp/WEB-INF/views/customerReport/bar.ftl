<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>潜在客户报表查询</title>
    <#--引入echart-->
    <script src="/js/plugins/echarts/echarts.common.min.js"></script>
    <div id="main" style="width: 600px;height: 400px;"></div>
    <script>
        var mychart=echarts.init(document.getElementById('main'));
            var option = {
                title: {
                    text: '潜在客户报表',
                    subtext: "分组类型:${groupType}"
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['潜在客户数']
                },
                toolbox: {
                    show: true,
                    feature: {
                        dataView: {show: true, readOnly: false},
                        magicType: {show: true, type: ['line', 'bar']},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                calculable: true,
                xAxis: [
                    {
                        type: 'category',
                        data: ${xList}
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: '潜在客户数',
                        type: 'bar',
                        data: ${yList},  //[8,15,30,3]
                        markPoint: {
                            data: [
                                {type: 'max', name: '最大值'},
                                {type: 'min', name: '最小值'}
                            ]
                        },
                        markLine: {
                            data: [
                                {type: 'average', name: '平均值'}
                            ]
                        }
                    }
                ]
            };
        mychart.setOption(option);
    </script>
</head>
<body>

</body>
</html>