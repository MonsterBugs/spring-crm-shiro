<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>潜在客户报表查询</title>
    <#--引入echart-->
    <script src="/js/plugins/echarts/echarts.common.min.js"></script>
    <div id="main" style="width: 600px;height: 400px;"></div>
    <script>
        var mychart=echarts.init(document.getElementById('main'));
        var option = {
            title: {
                text: '潜在客户报表',
                subtext: '分组类型:${groupType}',
                left: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: '{a} <br/>{b} : {c} ({d}%)'
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                data: ${groupTypeValues} //['孙总','钱二明',"赵一明","王五"]
            },
            series: [
                {
                    name: '潜在客户数',
                    type: 'pie',
                    radius: '55%',
                    center: ['50%', '60%'],
                    data: ${data},//[{value: 335, name: '孙总'}]
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        mychart.setOption(option);
    </script>
</head>
<body>

</body>
</html>