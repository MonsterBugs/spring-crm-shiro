﻿<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>客户列表管理</title>
    <#include "../common/link.ftl" >
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="customer"/>
    <#include "../common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>客户列表管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <form class="form-inline" id="searchForm" action="/customer/list.do" method="post">
                    <input type="hidden" name="currentPage" id="currentPage" value="1">
                    <div class="form-group">
                        <@shiro.hasAnyRoles name="ADMIN,Market_Manager">
                            <label for="keyword">关键字:</label>
                        <input type="text" class="form-control" id="keyword" name="keyword" value="${qo.keyword!}"
                               placeholder="请输入姓名/电话">
                    </div>
                    <div class="form-group">
                        <label for="dept"> 销售人员:</label>
                        <select class="form-control" id="sellerId" name="sellerId">
                            <option value="-1">全部</option>
                                <#list employees as e>
                                    <option value="${e.id}">${e.name!}</option>
                                </#list>
                        </select>
                    <#--回显-->
                        <script>
                            $("#sellerId").val(${qo.sellerId!});
                        </script>
                    </div>
                    <button id="btn_query" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>
                        查询
                    </button>
                    </@shiro.hasAnyRoles>
                    <!--编写内容-->
                    <div class="box-body table-responsive no-padding ">
                        <table class="table table-hover table-bordered">
                            <tr>
                                <th>编号</th>
                                <th>名称</th>
                                <th>电话</th>
                                <th>QQ</th>
                                <th>职业</th>
                                <th>来源</th>
                                <th>销售员</th>
                                <th>状态</th>
                                <th>录入时间</th>
                                <th>操作</th>
                            </tr>
                        <#list pageResult.list as customer>
                            <tr>
                            <#--空值处理-->
                                <td>${customer_index+1!}</td>
                                <td>${customer.name!}</td>
                                <td>${customer.tel!}</td>
                                <td>${customer.qq!}</td>
                                <td>${(customer.job.title)!}</td>
                                <td>${(customer.source.title)!}</td>
                                <td>${(customer.seller.name)!}</td>
                                <td>${customer.statusName!}</td>
                            <#--对于数据是日期格式,需要做以下处理-->
                                <td>${customer.inputTime?string('yyyy-MM-dd')}</td>
                                <td>
                                <#--可以直接获取json字符串,供模态框数据回显-->
                                    <a href="#" class="btn btn-info btn-xs btn-input"
                                       data-json='${customer.json}'>
                                        <span class="glyphicon glyphicon-pencil"></span> 吸纳
                                    </a>
                                    <a href="#" class="btn btn-info btn-xs btn-change"
                                       data-json='${customer.json}'>
                                        <span class="glyphicon glyphicon-pencil"></span> 移交
                                    </a>
                                </td>
                            </tr>
                        </#list>
                        </table>
                        <!--分页-->
                    <#include "../common/page.ftl">
                    </div>
            </div>
        </section>
    </div>
    <#include "../common/footer.ftl">
</div>
</body>
</html>