﻿<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>潜在客户管理</title>
    <#include "../common/link.ftl" >
    <script>
        $(function () {
            //触发客户添加编辑模态框事件
            $(".btn-input").click(function () {
                $('#editForm input').val('');
                //获取data属性的json字符串,可以直接j获取s对象
                var json = $(this).data('json');
                if (json) {
                    //使用层次选择器 过滤选择器 回显指定数据
                    $('#editForm input[name=id]').val(json.id)
                    $('#editForm input[name=name]').val(json.name)
                    $('#editForm input[name=name]').attr("readonly","readonly")
                    $('#editForm input[name=age]').val(json.age)
                    $('#editForm select[name=gender]').val(json.gender)
                    $('#editForm input[name=tel]').val(json.tel)
                    $('#editForm input[name=qq]').val(json.qq)
                    //当过滤选择器的属性值有特殊字符 . -等;属性值需要加上单引号
                    $("#editForm select[name='job.id']").val(json.jobId)
                    $("#editForm select[name='source.id']").val(json.sourceId)
                }
                //打开客户添加编辑模态框
                $('#editModal').modal('show');
            })


            //客户手机号码远程验证
            $("#editForm").bootstrapValidator(
                    {
                        feedbackIcons: { //图标
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        fields: { //配置要验证的字段
                            name: {
                                validators: { //验证的规则
                                    notEmpty: { //不能为空
                                        message: "客户名必填" //错误时的提示信息
                                    },
                                    stringLength: { //字符串的长度范围
                                        min: 1,
                                        max: 5
                                    }
                                }
                            },
                            age: {
                                validators: {
                                    between: { //数字的范围
                                        min: 0,
                                        max: 110
                                    }
                                }
                            },
                            tel: {
                                validators: {
                                    notEmpty: { //不能为空
                                        message: "电话号码必填" //错误时的提示信息
                                    },
                                    remote: { //远程验证
                                        type: 'POST', //请求方式
                                        url: '/customer/checkTel.do', //请求地址
                                        message: '用户名已经存在', //验证不通过时的提示信息
                                        delay: 2000, //输入内容2秒后发请求
                                        data: function () {  //自定义提交参数，默认只会提交当前用户名input的参数
                                            return {
                                                id: $('[name="id"]').val(),
                                                name: $('[name="name"]').val()
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }).on('success.form.bv', function () { //表单所有数据验证通过后执行里面的代码

                //提交异步表单
                $("#editForm").ajaxForm(handlerMessage)
            });

            //触发修改客户状态模态框事件
            $(".btn-change").click(function () {
                $('#statusForm input').val('');
                //获取data属性的json字符串,可以直接j获取s对象
                var json = $(this).data('json');
                //使用层次选择器 过滤选择器 回显指定数据
                $('#statusForm input[name=id]').val(json.id)
                $('#statusForm input[name=name]').val(json.name)
                $('#statusForm input[name=status]').val(json.status)
                //打开修改客户状态模态框
                $('#statusModal').modal('show');
            })
            //使用异步提交修改客户状态表单
            $(".trace-submit").click(function () {
                $("#traceForm").ajaxSubmit(handlerMessage)
            })


            //触发跟进模态框事件
            $(".btn-trace").click(function () {
                var json = $(this).data('json'); //json是客户对象
                $("#traceForm input[name='customer.name']").val(json.name)
                $("#traceForm input[name='customer.id']").val(json.id);//把客户id设置到跟进历史对象的客户id中
                $("#traceModal").modal('show');
            })
            //使用异步提交跟进状态表单
            $("#traceForm").ajaxForm()

            //跟进时间使用日期插件
            $("input[name=traceTime]").datepicker({
                language: "zh-CN", //语言
                autoclose: true, //选择日期后自动关闭
                todayHighlight: true //高亮今日日期
            });

            //移交模态框
            $(".btn-transfer").click(function () {
                var json = $(this).data('json'); //json是customer对象
                $("#transferForm input[name='customer.name']").val(json.name);
                $("#transferForm input[name='customer.id']").val(json.id);
                $("#transferForm input[name='oldSeller.name']").val(json.sellerName);
                $("#transferForm input[name='oldSeller.id']").val(json.sellerId);
                $("#transferModal").modal('show');
            })

            $(".transfer-Submit").click(function () {
                $("#transferForm").ajaxSubmit(handlerMessage)
            })
        })
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="customer_potential"/>
    <#include "../common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>潜在客户管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <form class="form-inline" id="searchForm" action="/customer/potentialList.do" method="post">
                    <input type="hidden" name="currentPage" id="currentPage" value="1">
                    <@shiro.hasAnyRoles name="ADMIN,Market_Manager">
                    <div class="form-group">
                        <label for="keyword">关键字:</label>
                        <input type="text" class="form-control" id="keyword" name="keyword" value="${qo.keyword!}"
                               placeholder="请输入姓名/电话">
                    </div>
                    <div class="form-group">
                        <label for="dept"> 销售人员:</label>
                        <select class="form-control" id="sellerId" name="sellerId">
                            <option value="-1">全部</option>
                                <#list sellers as e>
                                    <option value="${e.id}">${e.name!}</option>
                                </#list>
                        </select>
                        <script>
                            $("#sellerId").val(${qo.sellerId!});
                        </script>
                    </div>
                    <button id="btn_query" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>
                        查询
                    </button>
                    </@shiro.hasAnyRoles>
                    <input type="hidden" name="currentPage" id="currentPage" value="1">
                    <a href="#" class="btn btn-success btn-input" style="margin: 10px">
                        <span class="glyphicon glyphicon-plus"></span> 添加
                    </a>
                </form>
                <!--编写内容-->
                <div class="box-body table-responsive no-padding ">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <th>编号</th>
                            <th>名称</th>
                            <th>电话</th>
                            <th>QQ</th>
                            <th>职业</th>
                            <th>来源</th>
                            <th>销售员</th>
                            <th>状态</th>
                            <th>录入时间</th>
                            <th>操作</th>
                        </tr>
                        <#list pageResult.list as customer>
                            <tr>
                            <#--空值处理-->
                                <td>${customer_index+1!}</td>
                                <td>${customer.name!}</td>
                                <td>${customer.tel!}</td>
                                <td>${customer.qq!}</td>
                                <td>${(customer.job.title)!}</td>
                                <td>${(customer.source.title)!}</td>
                                <td>${(customer.seller.name)!}</td>
                                <td>${customer.statusName!}</td>
                            <#--对于数据是日期格式,需要做以下处理-->
                                <td>${customer.inputTime?string('yyyy-MM-dd')}</td>
                                <td>
                                <#--可以直接获取json字符串,供模态框数据回显-->
                                    <a href="#" class="btn btn-info btn-xs btn-input"
                                       data-json='${customer.json}'>
                                        <span class="glyphicon glyphicon-pencil"></span> 编辑
                                    </a>
                                    <a href="#" class="btn btn-danger btn-xs btn-trace" data-json='${customer.json}'>
                                        <span class="glyphicon glyphicon-phone"></span> 跟进
                                    </a>
                                    <!--管理员和经理才能看到该下拉框-->
                                    <@shiro.hasAnyRoles name="ADMIN,Market_Manager">
                                    <a href="#" class="btn btn-danger btn-xs btn-transfer" data-json='${customer.json}'>
                                        <span class="glyphicon glyphicon-phone"></span> 移交
                                    </a>
                                    </@shiro.hasAnyRoles>
                                    <a href="#" class="btn btn-danger btn-xs btn-change"
                                       data-json='${customer.json}'>
                                        <span class="glyphicon glyphicon-pencil"></span> 状态变更
                                    </a>
                                </td>
                            </tr>
                        </#list>
                    </table>
                    <!--分页-->
                    <#include "../common/page.ftl">
                </div>
            </div>
        </section>
    </div>
    <#include "../common/footer.ftl">
</div>
</body>
<!-- 客户编辑Modal -->
<div class="modal fade" id="editModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title inputTitle">客户编辑</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/customer/saveOrUpdate.do" method="post" id="editForm">
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户名称：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name"
                                   placeholder="请输入客户姓名"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户年龄：</label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" name="age"
                                   placeholder="请输入客户年龄"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户性别：</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="gender">
                                <option value="1">男</option>
                                <option value="0">女</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户电话：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="tel"
                                   placeholder="请输入客户电话"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户QQ：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="qq"
                                   placeholder="请输入客户QQ"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户工作：</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="job.id">
                                <#list jobs as j>
                                    <option value="${j.id}">${j.title}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户来源：</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="source.id">
                                <#list sources as s>
                                    <option value="${s.id}">${s.title}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-primary">保存</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<#--客户状态变更模态框-->
<div class="modal fade" id="statusModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title inputTitle">修改客户状态</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/customer/changeStatus.do" method="post" id="statusForm">
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户名称：</label>
                        <div class="col-sm-6"><#--客户名称不可修改-->
                            <input type="text" class="form-control" name="name" readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户状态：</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="status">
                                <option value="0">潜在客户</option>
                                <option value="2">开发失败</option>
                                <option value="4">移入客户池</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-primary">保存</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<#--客户跟进模态框-->
<div class="modal fade" id="traceModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">跟进</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/customerTraceHistory/saveOrUpdate.do" method="post"
                      id="traceForm">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">客户姓名：</label>
                        <div class="col-lg-6">
                            <input type="hidden" name="customer.id"/>
                            <input type="text" class="form-control" readonly name="customer.name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">跟进时间：</label>
                        <div class="col-lg-6 ">
                            <input type="text" class="form-control" name="traceTime" placeholder="请输入跟进时间">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">交流方式：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="traceType.id">
                                <#list ccts as c>
                                    <option value="${c.id}">${c.title}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">跟进结果：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="traceResult">
                                <option value="3">优</option>
                                <option value="2">中</option>
                                <option value="1">差</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">跟进记录：</label>
                        <div class="col-lg-6">
                            <textarea type="text" class="form-control" name="traceDetails"
                                      placeholder="请输入跟进记录" name="remark"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="submit" class="btn btn-primary trace-Submit">保存</button>
            </div>
        </div>
    </div>
</div>
<#--客户移交模态框-->
<div id="transferModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">移交</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/customerTransfer/saveOrUpdate.do" method="post" id="transferForm"
                      style="margin: -3px 118px">
                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">客户姓名：</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="customer.name" readonly>
                            <input type="hidden" class="form-control" name="customer.id">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sn" class="col-sm-4 control-label">旧营销人员：</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="oldSeller.name" readonly>
                            <input type="hidden" class="form-control" name="oldSeller.id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sn" class="col-sm-4 control-label">新营销人员：</label>
                        <div class="col-sm-8">
                            <select name="newSeller.id" class="form-control">
                                <#-->新营销人员为Market_ManagerMarket_Manager和Market-->
                            <#list sellers as e>
                                <option value="${e.id}">${e.name}</option>
                            </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sn" class="col-sm-4 control-label">移交原因：</label>
                        <div class="col-sm-8">
                            <textarea type="text" class="form-control" id="reason" name="reason" cols="10"></textarea>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary transfer-Submit">保存</button>
            </div>
        </div>
    </div>
</div
</html>