﻿<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>课程管理</title>
    <#include "../common/link.ftl" >
    <script>
        $(function () {
            //触发模态框事件
            $(".btn-input").click(function () {
                //清空数据
                $('#editForm input,#editForm select').val('');
                $("#editForm select[name='customer.id']").attr("disabled",false)
                //获取data属性的json字符串,可以直接j获取s对象
                var json = $(this).data('json');
                if (json) {
                    //使用层次选择器 过滤选择器 回显指定数据
                    $('#editForm input[name=id]').val(json.id)
                    $('#editForm input[name=money]').val(json. money)
                    $("#editForm select[name='course.id']").val(json.courseId)
                    $("#editForm select[name='customer.id']").attr("disabled",true)
                    $("#editForm select[name='customer.id']").val(json.customerId)
                }
                //打开模态框
                $('#inputModal').modal('show');
            })
            $('#save').click(function () {
                $("#editForm select[name='customer.id']").attr("disabled",false)
                //使用jQuery插件异步提交表单
                $('#editForm').ajaxSubmit(handlerMessage)

            })


        })
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="courseOrder"/>
    <#include "../common/menu.ftl">
<div class="content-wrapper">
    <section class="content-header">
    </section>
<section class="content">
<div class="box">
    <!--高级查询--->
<div style="margin: 10px;">
<form class="form-inline" id="searchForm" action="/courseOrder/list.do" method="post">
    <input type="hidden" name="currentPage" id="currentPage" value="1">
    <div class="form-group">
        <label for="keyword">关键字:</label>
        <input type="text" class="form-control" id="keyword" name="keyword" value="${qo.keyword!}"
               placeholder="请输入关键字">
    </div>
    <div class="form-group">
        <label for="dept"> 课程名称:</label>
        <select class="form-control" id="courseId" name="courseId">
            <option value="-1">全部</option>
                                <#list courses as course>
                                    <option value="${course.id}">${course.title}</option>
                                </#list>
        </select>
        <script>
            $("#courseId").val(${qo.courseId!});
        </script>
    </div>
    <button id="btn_query" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>
        查询
    </button>
                        <a href="#" class="btn btn-success btn_redirect btn-input">
                            <span class="glyphicon glyphicon-plus"></span> 添加
                        </a>
                </div>
                </form>
            <!--编写内容-->
            <div class="box-body table-responsive no-padding ">
                <table class="table table-hover table-bordered">
                    <tr>
                        <th>编号</th>
                        <th>客户名称</th>
                        <th>销售课程</th>
                        <th>销售时间</th>
                        <th>销售金额</th>
                        <th>操作</th>
                    </tr>
                        <#list pageResult.list as courseOrder>
                            <tr>
                            <#--空值处理-->
                                <td>${courseOrder_index+1!}</td>
                                <td>${(courseOrder.customer.name)!}</td>
                                <td>${(courseOrder.course.title)!}</td>
                                <td>${(courseOrder.inputTime?string('yyyy-MM-dd'))!}</td>
                                <td>${(courseOrder.money)!}</td>
                                <td>
                                <#--可以直接获取json字符串-->
                                    <a href="#" class="btn btn-info btn-xs btn-input" data-json='${courseOrder.json}'>
                                        <span class="glyphicon glyphicon-pencil"></span> 编辑
                                    </a>
                                </td>
                            </tr>
                        </#list>
                </table>
                <!--分页-->
                    <#include "../common/page.ftl">
            </div>
    </div>
    </section>
</div>
                            <#include "../common/footer.ftl">
</div>
</body>
            <!--编写内容-->
            <div class="box-body table-responsive no-padding ">
                <table class="table table-hover table-bordered">
                    <tr>
                        <th>编号</th>
                        <th>客户名称</th>
                        <th>销售课程</th>
                        <th>销售时间</th>
                        <th>销售金额</th>
                        <th>操作</th>
                    </tr>
                        <#list pageResult.list as courseOrder>
                            <tr>
                            <#--空值处理-->
                                <td>${courseOrder_index+1!}</td>
                                <td>${(courseOrder.customer.name)!}</td>
                                <td>${(courseOrder.courseOrder.name)!}</td>
                                <td>${(courseOrder.inputTime?string('yyyy-MM-dd'))!}</td>
                                <td>${(courseOrder.courseOrder.money)!}</td>
                                <td>
                                <#--可以直接获取json字符串-->
                                    <a href="#" class="btn btn-info btn-xs btn-input" data-json='${courseOrder.json}'>
                                        <span class="glyphicon glyphicon-pencil"></span> 编辑
                                    </a>
                                </td>
                            </tr>
                        </#list>
                </table>
                <!--分页-->
                    <#include "../common/page.ftl">
            </div>
    </div>
    </section>
</div>
                            <#include "../common/footer.ftl">
</div>
</body>
<!-- Modal -->
<div class="modal fade" id="inputModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">课程新增/编辑</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/courseOrder/saveOrUpdate.do" method="post" id="editForm">
                    <input type="hidden" name="id">
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label">用户名称：</label>
                        <
                        <div class="col-sm-6">
                            <select class="form-control" name="customer.id"id="customerName">
                                <#list coustomers as coustomer>
                                    <option value="${coustomer.id}">${coustomer.name}</option>
                                </#list>
                            </select>
                        </div>
                        <div class="form-group" style="margin-top: 10px;">
                            <label for="sn" class="col-sm-3 control-label">课程名称：</label>
                            <select class="form-control" name="course.id">
                                <#list courses as course>
                                    <option value="${course.id}">${course.title}</option>
                                </#list>
                            </select>
                        </div>
                        <div class="form-group" style="margin-top: 10px;">
                            <label for="sn" class="col-sm-3 control-label">金额：</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="money" name="money" "
                                placeholder="请输入金额">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="button" class="btn btn-primary" id="save">保存</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</html>