﻿<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>部门管理</title>
    <#include "../common/link.ftl" >
    <script>
        $(function () {
            //触发模态框事件
            $(".btn-input").click(function () {
                $('#editForm input').val('');
                //获取data属性的json字符串,可以直接j获取s对象
                var json = $(this).data('json');
                if (json) {
                    //使用层次选择器 过滤选择器 回显指定数据
                    $('#editForm input[name=id]').val(json.id)
                    $('#editForm input[name=name]').val(json.name)
                    $('#editForm input[name=sn]').val(json.sn)
                }
                //打开模态框
                $('#inputModal').modal('show');
            })

            //使用jQuery插件异步提交表单
            $('#editForm').ajaxForm(handlerMessage)

            //删除员工
            $('.btn-delete').click(function () {
                console.log(5);
                //获取员工id
                var id = $(this).data('id');
                console.log(id);
                $.get('/department/delete.do', {id: id}, handlerMessage)
            })
        })
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="department"/>
    <#include "../common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>部门管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <form class="form-inline" id="searchForm" action="/department/list.do" method="post">
                    <input type="hidden" name="currentPage" id="currentPage" value="1">
                    <a href="#" class="btn btn-success btn-input" style="margin: 10px">
                        <span class="glyphicon glyphicon-plus"></span> 添加
                    </a>
                </form>
                <!--编写内容-->
                <div class="box-body table-responsive no-padding ">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <th>编号</th>
                            <th>部门名称</th>
                            <th>部门编号</th>
                            <th>操作</th>
                        </tr>
                        <#list pageResult.list as department>
                            <tr>
                            <#--空值处理-->
                                <td>${department_index+1!}</td>
                                <td>${department.name!}</td>
                                <td>${department.sn!}</td>
                                <td>
                                <#--可以直接获取json字符串-->
                                    <a href="#" class="btn btn-info btn-xs btn-input" data-json='${department.json}'>
                                        <span class="glyphicon glyphicon-pencil"></span> 编辑
                                    </a>
                                    <a href="#" data-id='${department.id}'
                                       class="btn btn-danger btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span> 删除
                                    </a>
                                </td>
                            </tr>
                        </#list>
                    </table>
                    <!--分页-->
                    <#include "../common/page.ftl">
                </div>
            </div>
        </section>
    </div>
    <#include "../common/footer.ftl">
</div>
<!-- Modal -->
<div class="modal fade" id="inputModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">部门新增/编辑</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/department/saveOrUpdate.do" method="post" id="editForm">
                    <input type="hidden" name="id">
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label">名称：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="name" name="name" "
                            placeholder="请输入部门名">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="sn" class="col-sm-3 control-label">编码：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="sn" name="sn" "
                            placeholder="请输入部门编码">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="submit" class="btn btn-primary">保存</button>
            </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>