﻿<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>班级管理</title>
    <#include "../common/link.ftl" >
    <script>
        $(function () {
            //触发模态框事件
            $(".btn-input").click(function () {
                $('#editForm input').val('');
                //获取data属性的json字符串,可以直接j获取s对象
                var json=$(this).data('json');
                if(json){
                    //使用层次选择器 过滤选择器 回显指定数据
                    $('#editForm input[name=id]').val(json.id)
                    $('#editForm input[name=name]').val(json.name)
                    $('#editForm input[name=number]').val(json.number)
                    $('#editForm select[name=employee_id]').val(json.employee_id)
                    console.log(json);
                    console.log(json.id);
                    console.log(json.name);
                    console.log(json.number);
                    console.log(json.number);
                    console.log(json.employee_id);
                }

                //打开模态框
                $('#inputModal').modal('show');
            })

            //使用jQuery插件异步提交表单
            $('#editForm').ajaxForm(function (data) {
                if(data.success) {
                    $.messager.popup(data.msg)
                    //跳转到部门列表
                    location.href = '/classinfo/list.do';
                }else{
                    $.messager.alert(data.msg)
                }
            })

            //删除班级
            $('.btn-delete').click(function () {
                //获取班级id
                var id = $(this).data('id');
                console.log(id);
                $.get('/classinfo/delete.do',{id:id},handlerMessage)
            })
        })
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="classinfo"/>
    <#include "../common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>班级管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <div style="margin: 10px;">
                    <form class="form-inline" id="searchForm" action="/classinfo/list.do" method="post">
                        <input type="hidden" name="currentPage" id="currentPage" value="1">
                        <div class="form-group">
                            <label for="keyword">关键字:</label>
                            <input type="text" class="form-control" id="keyword" name="keyword" value="${qo.keyword!}" placeholder="请输入姓名/邮箱">
                        </div>
                        <div class="form-group">
                            <label for="classinfo"> 班主任:</label>
                            <select class="form-control" id="keyId" name="keyId">
                                <option value="-1">全部</option>
                                <#list employees as e>
                                    <option value="${e.id}">${e.name!}</option>
                                </#list>
                            </select>
                            <script>
                                $("#keyId").val(${qo.keyId!});
                            </script>
                        </div>
                        <button id="btn_query" class="btn btn-primary "><span class="glyphicon glyphicon-search"></span> 查询</button>
                        <a href="#" class="btn btn-success btn_redirect btn-input">
                            <span class="glyphicon glyphicon-plus"></span> 添加
                        </a>
                    </form>
                <!--编写内容-->
                <div class="box-body table-responsive no-padding ">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <th>编号</th>
                            <th>班级名称</th>
                            <th>班级人数</th>
                            <th>班主任</th>
                            <th>操作</th>
                        </tr>
                        <#list pageResult.list as classinfo>
                            <tr>
                            <#--空值处理-->
                                <td>${classinfo_index+1!}</td>
                                <td>${classinfo.name!}</td>
                                <td>${classinfo.number!}</td>
                                <td>${(classinfo.employee.name)!}</td>
                                <td>
                                    <a href="#" class="btn btn-info btn-xs btn-input" data-json='${classinfo.json}'>
                                        <span class="glyphicon glyphicon-pencil"></span> 编辑
                                    </a>
                                    <a href="#" data-id='${classinfo.id}'
                                       class="btn btn-danger btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span> 删除
                                    </a>
                                </td>
                            </tr>
                        </#list>
                    </table>
                    <!--分页-->
                    <#include "../common/page.ftl">
                </div>
            </div>
        </section>
    </div>
    <#include "../common/footer.ftl">
</div>
<!-- Modal -->
<div class="modal fade" id="inputModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">班级新增/编辑</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/classinfo/saveOrUpdate.do" method="post" id="editForm">
                    <input type="hidden" name="id">
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label">名称：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="name" name="name" "
                            placeholder="请输入班级名">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="sn" class="col-sm-3 control-label">人数：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="number" name="number" "
                            placeholder="请输入班级人数">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="sn" class="col-sm-3 control-label">班主任：</label>
                        <div class="col-sm-7">
                            <select class="form-control" id="employee_id" name="employee_id">
                                <option value="-1">全部</option>
                                <#list employees as e>
                                    <option value="${e.id}">${e.name!}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="submit" class="btn btn-primary">保存</button>
            </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
