﻿<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>公告通知管理</title>
    <#include "../common/link.ftl" >
    <script>
        $(function () {
            //公告模态框
            $(".btn-input").click(function () {
                //清空数据
                $('#editForm input').val('');
                $('#editForm textarea').val('');
                var json = $(this).data('json'); //json是notice对象
                if (json) {      //json有数据,代表编辑
                    console.log(json);
                    $("#editForm input[name=id]").val(json.id);
                    $("#editForm input[name=title]").val(json.title);
                    $("#editForm textarea[name=content]").val(json.content);
                }
                $("#editModal").modal('show');
            })
            //使用jQuery插件异步提交表单
            $('#editForm').ajaxForm(handlerMessage)

            //删除
            $('.btn-delete').click(function () {
                var json = $(this).data('json');
                var id = json.id;
                console.log(id);
                $.get("/notice/delete.do", {id: id}, handlerMessage);
            })
        })
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="notice"/>
    <#include "../common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>公告栏</h1>
        </section>
        <section class="content">
            <div class="box">
                <form class="form-inline" id="searchForm" action="/notice/list.do" method="post">
                    <input type="hidden" name="currentPage" id="currentPage" value="1">
                    <a href="#" class="btn btn-success btn-input" style="margin: 10px">
                        <span class="glyphicon glyphicon-plus"></span> 添加
                    </a>
                </form>
            <div class="box">
                <!--编写内容-->
                <div class="box-body table-responsive no-padding ">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <th>编号</th>
                            <th>公告标题</th>
                            <th>发布人</th>
                            <th>发布时间</th>
                            <th>是否已读</th>
                            <th>操作</th>
                        </tr>
                        <#list pageResult.list as notice>
                            <tr>
                            <#--空值处理-->
                                <td>${notice_index+1!}</td>
                                <td>${notice.title!}</td>
                                <td>${notice.issuer.name!}</td>
                                <td>${notice.pubdate!?string('yyyy-MM-dd')}</td>
                                <td>${notice.read?string("已读","未读")}</td>
                                <td>
                                    <a href="/notice/get.do?id=${notice.id}" class="btn btn-info btn-xs btn-look"
                                       data-json='${notice.json}'>
                                        <span class="glyphicon glyphicon-pencil"></span> 查看
                                    </a>
                                    <a href="#" class="btn btn-success btn-xs btn-input" data-json='${notice.json}'>
                                        <span class="glyphicon glyphicon-phone"></span> 编辑
                                    </a>
                                    <a href="#" class="btn btn-danger btn-xs btn-delete" data-json='${notice.json}'>
                                        <span class="glyphicon glyphicon-phone"></span> 删除
                                    </a>
                                </td>
                            </tr>
                        </#list>
                    </table>
                    <!--分页-->
                    <#include "../common/page.ftl">
                </div>
            </div>
        </section>
    </div>
    <#include "../common/footer.ftl">
</div>
</body>
<!-- 公告新增/编辑Modal -->
<div class="modal fade" id="editModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title inputTitle">公告新增/编辑</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/notice/saveOrUpdate.do" method="post" id="editForm">
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">公告标题：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="title"
                                   placeholder="请输入公告标题"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">公告内容：</label>
                        <div class="col-sm-6">
                            <textarea type="text" class="form-control" id="content" name="content" cols="10"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-primary">保存</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</html>