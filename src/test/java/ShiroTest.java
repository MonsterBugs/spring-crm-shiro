import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

public class ShiroTest {
    @Test
    public void shiroTest() {
        //记载shiro配置文件
        IniSecurityManagerFactory factory = new IniSecurityManagerFactory(("classpath:shiro.ini"));
        //创建安全管理器
        SecurityManager manager = factory.getInstance();
        //将安全管理器添加到运行环境
        SecurityUtils.setSecurityManager(manager);
        //获取用户登录对象
        Subject subject = SecurityUtils.getSubject();
        //获取当前登录状态
        System.out.println("登录状态前"+subject.isAuthenticated());
        //创建登录用户的身份凭证
        UsernamePasswordToken token = new UsernamePasswordToken("zhangsan", "555");
        //用户登录
        subject.login(token);
        System.out.println("是否登录成功"+subject.isAuthenticated());
        //用户注销
        subject.logout();
        System.out.println("注销后的认证状态"+subject.isAuthenticated());


    }


}
