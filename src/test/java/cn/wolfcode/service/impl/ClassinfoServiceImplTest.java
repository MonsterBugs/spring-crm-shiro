package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Classinfo;
import cn.wolfcode.query.ClassinfoQuery;
import cn.wolfcode.service.IClassinfoService;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class ClassinfoServiceImplTest {
    @Autowired
    private IClassinfoService service;
    @Test
    public void deleteByPrimaryKey() {
    }


    @Test
    public void insert() {
    }

    @Test
    public void selectByPrimaryKey() {
        Classinfo classinfo = service.selectByPrimaryKey(3L);
        System.out.println(classinfo.getJson());

    }

    @Test
    public void selectAll() {
        List<Classinfo> list = service.selectAll();
        System.out.println(list);
    }

    @Test
    public void updateByPrimaryKey() {
    }

    @Test
    public void queryForResult() {
        ClassinfoQuery qo = new ClassinfoQuery();
        PageInfo<Classinfo> pageinfo = service.queryForResult(qo);
        System.out.println(pageinfo);
    }
}