package cn.wolfcode.service.impl;

import cn.wolfcode.domain.CourseOrder;
import cn.wolfcode.query.CourseOrderQuery;
import cn.wolfcode.service.ICourseOrderService;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class CourseOrderServiceImplTest {
    @Autowired
    private ICourseOrderService service;
    @Test
    public void query() {
        CourseOrderQuery qo = new CourseOrderQuery();
        qo.setParentId(4L);
        PageInfo<CourseOrder> query = service.query(qo);
        List<CourseOrder> list = query.getList();
        for (CourseOrder courseOrder : list) {
            System.out.println(courseOrder);
        }


    }
}