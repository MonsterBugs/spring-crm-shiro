package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Role;
import cn.wolfcode.service.IRoleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class RolementServiceImplTest {
    @Autowired
    private IRoleService service;
    @Test
    public void deleteByPrimaryKey() {

    }

    @Test
    public void insert() {
    }

    @Test
    public void selectByPrimaryKey() {
        Role role = service.selectByPrimaryKey(1L);
        System.out.println(role);

    }

    @Test
    public void updateByPrimaryKey() {
    }

    @Test
    public void queryForResult() {
    }

    @Test
    public void queryForcount() {
    }

    @Test
    public void listAll() {
        List<Role> roles = service.listAll();
        System.out.println(roles);

    }
}