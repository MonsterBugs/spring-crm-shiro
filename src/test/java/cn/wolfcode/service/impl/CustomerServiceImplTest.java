package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Customer;
import cn.wolfcode.query.CustomerQuery;
import cn.wolfcode.query.Query;
import cn.wolfcode.service.ICustomerService;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class CustomerServiceImplTest {
    @Autowired
    private ICustomerService service;

    @Test
    public void query() {
        CustomerQuery query = new CustomerQuery();
        query.setStatus(0);
        query.setSellerId(5L);
        query.setKeyword("17");
        PageInfo<Customer> pageInfo = service.query(query);
        List<Customer> list = pageInfo.getList();
        for (Customer customer : list) {
            System.out.println(customer);
        }
    }
}