package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Department;
import cn.wolfcode.domain.Notice;
import cn.wolfcode.domain.Notice_Employee;
import cn.wolfcode.mapper.Notice_EmployeeMapper;
import cn.wolfcode.query.Query;
import cn.wolfcode.service.IDepartmentService;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class DepartmentServiceImplTest {
    @Autowired
    private IDepartmentService service;
    @Autowired
    private NoticeServiceImpl noticeService;
    @Autowired
    private Notice_EmployeeMapper ne;

    @Test
    public void deleteByPrimaryKey() {
    }

    @Test
    public void insert() {
        Department department = new Department();
        department.setName("事业部");
        department.setSn("SY");
        service.insert(department);
    }

    @Test
    public void selectByPrimaryKey() {
        Department department = service.selectByPrimaryKey(2L);
        System.out.println(department);
    }

    @Test
    public void updateByPrimaryKey() {
        Department department = new Department();
        department.setName("事业部");
        department.setSn("SY");
        department.setId(2L);
        service.updateByPrimaryKey(department);
    }
}