package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.mapper.EmployeeMapper;
import cn.wolfcode.query.EmployeeQuery;
import cn.wolfcode.service.IEmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class EmployeeServiceImplTest {
    @Autowired
    private IEmployeeService service;
    @Autowired
    private EmployeeMapper employeeMapper;

    @Test
    public void deleteByPrimaryKey() {
        service.deleteByPrimaryKey(3L);
    }

    @Test
    public void insert() {
        Employee employee = new Employee();
        employee.setName("许嵩");
        employee.setPassword("vae2020");
        employee.setAdmin(true);
    }

    @Test
    public void selectByPrimaryKey() {
        Employee employee = service.selectByPrimaryKey(16L);
        System.out.println(employee);
    }

    @Test
    public void updateByPrimaryKey() {
        Employee employee = new Employee();
        employee.setId(3L);
        employee.setName("许嵩");
        employee.setPassword("vae2020");
        employee.setAdmin(true);
    }

    @Test
    public void queryForResult() {
        EmployeeQuery qo = new EmployeeQuery();
    }

    @Test
    public void queryForcount() {
        EmployeeQuery qo = new EmployeeQuery();
        qo.setKeyword("xiao");
        System.out.println(qo);

    }


    @Test
    public void login() {
        Employee employee = employeeMapper.login("吴六明", "1");
        System.out.println();
    }

    @Test
    public void selectByName() {
        Employee employee = employeeMapper.selectByName("钱总");
        System.out.println(employee);

    }
}